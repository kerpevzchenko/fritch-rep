package com.fcfm.fritch.fragments

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.fcfm.fritch.MainActivity2
import com.fcfm.fritch.R
import com.fcfm.fritch.UserApplication
import com.fcfm.fritch.adapters.recycler_view.IngredientsListAdapter
import com.fcfm.fritch.fragments.recipes.Search_recipes
import com.fcfm.fritch.interfaces.IFragments
import com.fcfm.fritch.interfaces.IRVOnClick
import com.fcfm.fritch.models.Ingredient
import com.fcfm.fritch.utils.Category
import kotlinx.android.synthetic.main.fragment_cupboard.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Cupboard.newInstance] factory method to
 * create an instance of this fragment.
 */
class Cupboard : Fragment(), IRVOnClick.Ingredient, View.OnClickListener {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private var adapter : IngredientsListAdapter? = null

    private var ingredientsArray = mutableListOf<Ingredient>()

    private var frags: IFragments? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }

        //initializeIngredientsArray()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        frags = requireActivity() as MainActivity2
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_cupboard, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ingredientsArray = Ingredient.getAll(Category.CUPBOARD)

        c_rcv.layoutManager = LinearLayoutManager(requireContext())
        this.adapter = IngredientsListAdapter(this.requireContext(), ingredientsArray, this
        ) { searchRecipeByIngredient(it) }

        c_rcv.adapter = this.adapter

        cAdd_btn.setOnClickListener(this)

    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Cupboard.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Cupboard().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun onClick(ingredient: Ingredient) {
        val frag = ViewIngredient.newInstance(ingredient.id_ingredient!!)
        frags!!.replaceFragment(frag)
    }

    //Click en los elementos del fragment
    override fun onClick(v: View?) {
        v?.let{
            when(it.id)
            {
                R.id.cAdd_btn -> {
                    (requireActivity() as IFragments).replaceFragment(Add_ingredient(Category.CUPBOARD))
                }

            }
        }
    }

    private fun searchRecipeByIngredient(ingredient: Ingredient){
        Toast.makeText(requireActivity(), ingredient.name, Toast.LENGTH_SHORT).show()
        val frag = Search_recipes.newInstance(ingredient.id_ingredient!!)
        frags!!.replaceFragment(frag)
    }
}