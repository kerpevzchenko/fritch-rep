package com.fcfm.fritch.fragments.recipes

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fcfm.fritch.MainActivity2
import com.fcfm.fritch.R
import com.fcfm.fritch.UserApplication.Companion.pref
import com.fcfm.fritch.adapters.recycler_view.SearchIngredientAdapter
import com.fcfm.fritch.adapters.recycler_view.SelectIngredientsListAdapter
import com.fcfm.fritch.interfaces.IFragments
import com.fcfm.fritch.interfaces.IRVOnClick
import com.fcfm.fritch.models.Ingredient
import com.fcfm.fritch.models.Recipe
import com.fcfm.fritch.models.User
import com.fcfm.fritch.utils.Category
import com.fcfm.fritch.utils.UtilF
import kotlinx.android.synthetic.main.fragment_view_ingredients.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_ID = "id"
private const val ARG_DOWNLOADED = "downloaded"

/**
 * A simple [Fragment] subclass.
 * Use the [View_ingredients.newInstance] factory method to
 * create an instance of this fragment.
 */
class View_ingredients : Fragment(), IRVOnClick.Ingredient, View.OnClickListener {
    // TODO: Rename and change types of parameters
    private var id_recipe: Int = 0
    private var downloaded: Boolean = true

    private var ingredients = mutableListOf<Ingredient>()
    private var adapter: SelectIngredientsListAdapter? = null

    private var recipe : Recipe? = null

    private var frag : IFragments? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            id_recipe = it.getInt(ARG_ID)
            downloaded = it.getBoolean(ARG_DOWNLOADED)
        }

        if(!downloaded){
            Recipe.getRecipe(id_recipe,
                {
                    recipe = it
                    recipe!!.getIngredientsAPI(
                        { list ->
                            recipe!!.ingredients = list as MutableList<Ingredient>?
                            setDataToView()
                            hideLoading()
                        }
                        ,{
                            Log.e("ViewIngredientError", it.toString())
                        })
                },
                {
                    Log.e("ViewIngredientError", it.toString())
                })
        }
        else{
            recipe = Recipe.getRecipe(id_recipe)
        }


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_view_ingredients, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        frag = (requireActivity() as MainActivity2)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        showLoading()

        visDownloads_btn.setOnClickListener(this)
        visDel_btn.setOnClickListener(this)

        vis_rcv.layoutManager = LinearLayoutManager(requireContext())
        this.adapter = SelectIngredientsListAdapter(ingredients, false)
        vis_rcv.adapter = this.adapter

        if(downloaded) visDel_btn.visibility = View.VISIBLE

        if(downloaded) {
            setDataToView()
            hideLoading()
        }

        recipe?.let {
            setDataToView()
            if( ingredients.size > 0 ) this.adapter!!.notifyDataSetChanged()
        }

    }

    private fun setDataToView(){

        recipe?.GetDownloads({
            visDownloads_txt.text = it.toString() + " " + requireContext().getString(R.string.downloads)
        }, {})

        visName_txt.text = recipe!!.name
        visPortions_txt.text = recipe!!.portions.toString()

        val typesArray = requireContext().resources.getStringArray( R.array.recipe_types )
        visType_txt.text = typesArray[recipe!!.type!!]

        visDownloads_txt.text = recipe!!.downloads.toString() + " " + requireContext().getString(R.string.downloads)
        visDownloads_btn.isClickable = !downloaded

        visCode_txt.text = recipe!!.code

        visImg_iv.setImageBitmap(UtilF.base64ToBitmap(recipe!!.img!!))

        ingredients.clear()
        for(item in recipe!!.ingredients!!){
            ingredients.add(item)
        }
        if( ingredients.size > 0 ) this.adapter!!.notifyDataSetChanged()
    }

    private fun handleSuccess(list: List<Ingredient>){
        for (item in list){
            ingredients!!.add(item)
        }
    }

    private fun handleError(t:Throwable){
        (requireActivity() as MainActivity2).tostError(t)
    }

    private fun download(){
        showLoading()
        val save = Recipe.SaveData(recipe!!.id_recipe, pref.getInt(pref.USER))
        recipe!!.Download(save,
            {
                if(it == 1){
                    recipe!!.download(pref.getInt(pref.USER))
                    hideLoading()
                    frag!!.prevFragment()
                }
                else{
                    Log.e("Error", "Download recipe API")
                    hideLoading()
                }
            },
            {
                Log.e("Error", "Download recipe API")
                hideLoading()
            })



    }

    private fun showLoading(){
        vis_pb.visibility = View.VISIBLE
        vis_consl.visibility = View.INVISIBLE
    }

    private fun hideLoading(){
        vis_pb.visibility = View.INVISIBLE
        vis_consl.visibility = View.VISIBLE
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment View_ingredients.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(id: Int, downloaded: Boolean) =
            View_ingredients().apply {
                arguments = Bundle().apply {
                    putInt(ARG_ID, id)
                    putBoolean(ARG_DOWNLOADED, downloaded)
                }
            }
    }

    override fun onClick(ingredient: Ingredient) {
        TODO("Not yet implemented")
    }

    override fun onClick(v: View?) {
        v?.let{
            when(v.id){
                R.id.visDownloads_btn -> download()
                R.id.visDel_btn -> handleDeleteClick()
            }
        }
    }

    private fun handleDeleteClick() {
        //Toast.makeText(requireContext(), "Delete", Toast.LENGTH_SHORT).show()

        val dialog = AlertDialog.Builder(requireContext())
        dialog.setMessage(R.string.want_to_delete).
        setPositiveButton(R.string.yes, DialogInterface.OnClickListener{
                dialog, which ->
            showLoading()
            val rec = Recipe(id_recipe = recipe!!.id_recipe, user = User(id_user = pref.getInt(pref.USER)))

            rec.deleteSaved(
                {
                    hideLoading()
                    if(it == 1){
                        if(rec!!.delete(downloaded)){
                            hideLoading()
                            frag!!.prevFragment()
                        }
                    }
                },
                {
                    Toast.makeText(requireContext(), it.toString(), Toast.LENGTH_SHORT).show()
                    hideLoading()
                })

        }).
        setNegativeButton(R.string.no, DialogInterface.OnClickListener{
                dialog, which ->
        })

        dialog.show()


    }
}