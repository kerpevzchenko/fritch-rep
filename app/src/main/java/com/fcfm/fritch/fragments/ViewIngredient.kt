package com.fcfm.fritch.fragments

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Spinner
import android.widget.Toast
import com.fcfm.fritch.MainActivity2
import com.fcfm.fritch.R
import com.fcfm.fritch.UserApplication.Companion.pref
import com.fcfm.fritch.adapters.ViewRecipeViewPageAdapter
import com.fcfm.fritch.interfaces.IFragments
import com.fcfm.fritch.models.Ingredient
import com.fcfm.fritch.utils.Category
import kotlinx.android.synthetic.main.fragment_view_ingredient.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val PARAM_ID = "id"

/**
 * A simple [Fragment] subclass.
 * Use the [ViewIngredient.newInstance] factory method to
 * create an instance of this fragment.
 */
class ViewIngredient : Fragment(), View.OnClickListener, AdapterView.OnItemSelectedListener {
    // TODO: Rename and change types of parameters
    private var paramId: Int? = null

    var ingredient: Ingredient? = null

    private var frag : IFragments? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            paramId = it.getInt(PARAM_ID)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_view_ingredient, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        frag = (requireActivity() as MainActivity2)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ingredient = Ingredient.get(paramId!!)
        ingredient!!.id_account = pref.getInt(pref.ACCOUNT)
        setDataToView()

        viInc_btn.setOnClickListener(this)
        viDec_btn.setOnClickListener(this)
        viDone_btn.setOnClickListener(this)
        viRemove_btn.setOnClickListener(this)

        viCate_spin.onItemSelectedListener = this
    }

    private fun setDataToView(){
        viTitle_txt.text = ingredient!!.name
        viAmount_txt.setText(ingredient!!.amount.toString())
        viUnit_spin.setSelection(ingredient!!.unit!!)
        viCate_spin.setSelection(ingredient!!.category!!)

        if(ingredient!!.category == Category.FRIDGE){
            viType_lbl.visibility = View.VISIBLE
            viType_spin.visibility = View.VISIBLE
            viType_spin.setSelection(ingredient!!.type!!)
        }

        if(ingredient!!.category == Category.SEASONING){
            viUnit_spin.visibility = View.INVISIBLE
        }

        when(ingredient!!.category){
            0 -> {viImg_iv.setImageDrawable(requireContext().getDrawable(R.drawable.ic_icono_menu_condimentos))}
            1 -> {viImg_iv.setImageDrawable(requireContext().getDrawable(R.drawable.ic_icono_menu_alacena))}
            2 -> {
                when(ingredient!!.type){
                    0 -> {viImg_iv.setImageDrawable(requireContext().getDrawable(R.drawable.ic_icono_proteina))}
                    1 -> {viImg_iv.setImageDrawable(requireContext().getDrawable(R.drawable.ic_icono_vegetales))}
                    2 -> {viImg_iv.setImageDrawable(requireContext().getDrawable(R.drawable.ic_icono_frutas))}
                    3 -> {viImg_iv.setImageDrawable(requireContext().getDrawable(R.drawable.ic_icono_salsas))}
                    4 -> {viImg_iv.setImageDrawable(requireContext().getDrawable(R.drawable.ic_icono_porcion))}
                    5 -> {viImg_iv.setImageDrawable(requireContext().getDrawable(R.drawable.ic_icono_liquidos))}
                }
            }
        }
    }

    private fun addToAmount(toAdd : Int){
        ingredient!!.amount = ingredient!!.amount?.plus(toAdd)

        viAmount_txt.setText(ingredient!!.amount.toString())
    }

    private fun handleDoneClick(){
        showProgressBar()
        getDataFormView()
        if(ingredient!!.update()){
            ingredient!!.online = false
            ingredient!!.id_account = pref.getInt(pref.ACCOUNT)
            ingredient!!.update(
                {
                    if(it == 1){
                        ingredient!!.online = true
                        ingredient!!.updateOnline()
                        hideProgressBar()
                        Toast.makeText(requireContext(), R.string.saved, Toast.LENGTH_SHORT).show()
                        frag?.prevFragment()
                    }
                    else{
                        hideProgressBar()
                    }

                },
                {
                    Log.e("updateIngredientError", it.toString())
                    Toast.makeText(requireContext(), it.toString(), Toast.LENGTH_SHORT).show()
                    hideProgressBar()
                })
        }

    }

    private fun getDataFormView(){
        ingredient!!.unit = viUnit_spin.selectedItemPosition
        ingredient!!.category = viCate_spin.selectedItemPosition
        ingredient!!.amount = viAmount_txt.text.toString().toInt()

        if(ingredient!!.category == Category.FRIDGE){
            ingredient!!.type = viType_spin.selectedItemPosition
        }
        else{
            ingredient!!.type = null
        }

        ingredient!!.id_account = pref.getInt(pref.ACCOUNT)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ViewIngredient.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(paramId: Int) =
            ViewIngredient().apply {
                arguments = Bundle().apply {
                    putInt(PARAM_ID, paramId)
                }
            }
    }

    override fun onClick(v: View?) {
        v?.let {
            when(v.id){
                R.id.viInc_btn -> addToAmount(1)
                R.id.viDec_btn -> addToAmount(-1)
                R.id.viDone_btn -> handleDoneClick()
                R.id.viRemove_btn -> handleRemoveClick()
            }
        }
    }

    private fun handleRemoveClick() {
        //Toast.makeText(requireContext(), "Remove", Toast.LENGTH_SHORT).show()

        val dialog = AlertDialog.Builder(requireContext())
        dialog.setMessage(R.string.want_to_delete).
        setPositiveButton(R.string.yes, DialogInterface.OnClickListener{
            dialog, which ->
            /*if(ingredient!!.remove())
                frag!!.prevFragment()*/
            showProgressBar2()
            ingredient!!.remove(
                {
                    if(it == 1){
                        if(ingredient!!.remove())
                            hideProgressBar2()
                            frag!!.prevFragment()
                    }
                    else{
                        hideProgressBar2()
                    }
                },
                {
                    Toast.makeText(requireContext(), it.toString(), Toast.LENGTH_SHORT).show()
                    hideProgressBar2()
                })
        }).
        setNegativeButton(R.string.no, DialogInterface.OnClickListener{
            dialog, which ->
        })

        dialog.show()

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        parent?.let {
            when(parent.id){
                R.id.viCate_spin -> {
                    if(position == Category.FRIDGE){
                        viType_lbl.visibility = View.VISIBLE
                        viType_spin.visibility = View.VISIBLE
                    }
                    else{
                        viType_lbl.visibility = View.INVISIBLE
                        viType_spin.visibility = View.INVISIBLE
                    }

                }
            }
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        TODO("Not yet implemented")
    }

    private fun showProgressBar(){
        viDone_btn.visibility = View.INVISIBLE
        vi_pb.visibility = View.VISIBLE
    }

    private fun hideProgressBar(){
        viDone_btn.visibility = View.VISIBLE
        vi_pb.visibility = View.INVISIBLE
    }

    private fun showProgressBar2(){
        viRemove_btn.visibility = View.INVISIBLE
        vi_pb2.visibility = View.VISIBLE
    }

    private fun hideProgressBar2(){
        viRemove_btn.visibility = View.VISIBLE
        vi_pb2.visibility = View.INVISIBLE
    }
}