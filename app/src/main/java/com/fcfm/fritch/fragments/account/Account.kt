package com.fcfm.fritch.fragments.account

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.addTextChangedListener
import androidx.core.widget.doOnTextChanged
import com.fcfm.fritch.MainActivity
import com.fcfm.fritch.MainActivity2
import com.fcfm.fritch.R
import com.fcfm.fritch.UserApplication.Companion.pref
import com.fcfm.fritch.classes.MyWatcher
import com.fcfm.fritch.api.AccountAPI
import com.fcfm.fritch.api.UserAPI
import com.fcfm.fritch.fragments.login.Choose_user
import com.fcfm.fritch.interfaces.IFragments
import com.fcfm.fritch.models.User
import kotlinx.android.synthetic.main.fragment_account.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Account.newInstance] factory method to
 * create an instance of this fragment.
 */
class Account : Fragment(), View.OnClickListener {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    var userNameWatcher : MyWatcher? = null
    var passWatcher : MyWatcher? = null
    private var frag : IFragments? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_account, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        frag = (requireActivity() as MainActivity2)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        aSaveUsername_btn.setOnClickListener(this)

        aAccount_btn.setOnClickListener(this)

        aChangeUser_btn.setOnClickListener(this)

        aLogout_btn.setOnClickListener(this)

        val username = pref.getString(pref.USERNAME)
        aUsername_txt.setText(username)
        userNameWatcher = MyWatcher(aUsername_txt.text.toString()) {
            if(it) aSaveUsername_btn.visibility = View.INVISIBLE
            else aSaveUsername_btn.visibility = View.VISIBLE
        }
        aUsername_txt.addTextChangedListener(userNameWatcher)


        aEmail_txt.setText(pref.getString(pref.EMAIL))

        aPass_txt.setText("")
        passWatcher = MyWatcher(aPass_txt.text.toString()){
            if(it) aAccount_btn.visibility = View.INVISIBLE
            else aAccount_btn.visibility = View.VISIBLE

        }
        aPass_txt.addTextChangedListener(passWatcher)
    }

    private fun handleChangeUsernameClick(){
        showProgressBarUsername()
        val user = User(pref.getInt(pref.USER), aUsername_txt.text.toString(), pref.getInt(pref.ACCOUNT))
        user.updateAPI(
            {
                if(it == 1){
                    pref.save(pref.USERNAME, aUsername_txt.text.toString())
                    Toast.makeText(requireContext(), R.string.saved, Toast.LENGTH_SHORT).show()
                }
                else Toast.makeText(requireContext(), R.string.no_changes, Toast.LENGTH_SHORT).show()

                hideProgressBarUsername()
            },
            {
                Toast.makeText(requireContext(), it.toString(), Toast.LENGTH_SHORT).show()
                hideProgressBarUsername()
            })
    }

    private fun showProgressBarUsername(){
        aSaveUsername_btn.visibility = View.INVISIBLE
        aUsername_pb.visibility = View.VISIBLE
    }

    private fun hideProgressBarUsername(){
        aSaveUsername_btn.visibility = View.VISIBLE
        aUsername_pb.visibility = View.INVISIBLE

    }

    private fun handleChangePassClick(){
        showProgressBarPass()
        val account = com.fcfm.fritch.models.Account(pref.getInt(pref.ACCOUNT), null, aPass_txt.text.toString())
        account.update({
            if(it == 1) Toast.makeText(requireContext(), R.string.saved, Toast.LENGTH_SHORT).show()
            else Toast.makeText(requireContext(), R.string.no_changes, Toast.LENGTH_SHORT).show()
            hideProgressBarPass()
        },
        {
            Toast.makeText(requireContext(), it.toString(), Toast.LENGTH_SHORT).show()
            hideProgressBarPass()
        })
    }

    private fun showProgressBarPass(){
        aAccount_btn.visibility = View. INVISIBLE
        aAccount_pb.visibility = View.VISIBLE
    }

    private fun hideProgressBarPass(){
        aAccount_btn.visibility = View. VISIBLE
        aAccount_pb.visibility = View.INVISIBLE
    }

    override fun onClick(v: View?) {
        v?.let {
            when(v.id){
                R.id.aSaveUsername_btn -> handleChangeUsernameClick()
                R.id.aAccount_btn -> handleChangePassClick()
                R.id.aChangeUser_btn -> handleChangeUserClick()
                R.id.aLogout_btn -> handleLogoutClick()
            }
        }
    }

    private fun handleLogoutClick() {
        pref.logOut()
        frag!!.clearStack()
        val intent = Intent(requireContext(), MainActivity::class.java)
        startActivity(intent)
    }

    private fun handleChangeUserClick() {
        val frag = Choose_user.newInstance(Choose_user.ACCOUNT)

        val activity = requireActivity()
        if(activity is MainActivity){
            activity.replaceFragment(frag)
        } else if (activity is MainActivity2){
            activity.replaceFragment(frag)
        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Account.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Account().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}