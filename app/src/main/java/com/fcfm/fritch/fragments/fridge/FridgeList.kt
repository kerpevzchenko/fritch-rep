package com.fcfm.fritch.fragments.fridge

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.fcfm.fritch.R
import com.fcfm.fritch.adapters.recycler_view.IngredientsListAdapter
import com.fcfm.fritch.fragments.Add_ingredient
import com.fcfm.fritch.fragments.ViewIngredient
import com.fcfm.fritch.fragments.recipes.Search_recipes
import com.fcfm.fritch.interfaces.IFragments
import com.fcfm.fritch.interfaces.IRVOnClick
import com.fcfm.fritch.models.Ingredient
import com.fcfm.fritch.utils.Category
import kotlinx.android.synthetic.main.fragment_fridge_list.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [FridgeList.newInstance] factory method to
 * create an instance of this fragment.
 */
class FridgeList(val category: Int, val type: Int? = null) : Fragment(), View.OnClickListener, IRVOnClick.Ingredient {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private var frags: IFragments? = null

    private var adapter : IngredientsListAdapter? = null

    private var ingredientsArray = mutableListOf<Ingredient>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        frags = requireActivity() as IFragments
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fridge_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ingredientsArray = Ingredient.getAll(category, type)

        fl_rcv.layoutManager = LinearLayoutManager(requireContext())
        this.adapter = IngredientsListAdapter(this.requireContext(), ingredientsArray, this){searchRecipeByIngredient(it)}
        fl_rcv.adapter = this.adapter

        flAdd_btn.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        v?.let {
            when(v.id){
                R.id.flAdd_btn -> frags!!.replaceFragment(Add_ingredient(category, type))
            }
        }
    }

    override fun onClick(ingredient: Ingredient) {
        val frag = ViewIngredient.newInstance(ingredient.id_ingredient!!)
        frags!!.replaceFragment(frag)
    }

    private fun searchRecipeByIngredient(ingredient: Ingredient){
        Toast.makeText(requireActivity(), ingredient.name, Toast.LENGTH_SHORT).show()
        val frag = Search_recipes.newInstance(ingredient.id_ingredient!!)
        frags!!.replaceFragment(frag)
    }

    /*companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FridgeList.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            FridgeList().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }*/
}