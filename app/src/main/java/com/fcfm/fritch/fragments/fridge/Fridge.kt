package com.fcfm.fritch.fragments.fridge

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.fcfm.fritch.R
import com.fcfm.fritch.adapters.FridgeViewPageAdapter
import com.fcfm.fritch.fragments.Add_ingredient
import com.fcfm.fritch.interfaces.IFragments
import com.fcfm.fritch.utils.Category
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.fragment_fridge.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Fridge.newInstance] factory method to
 * create an instance of this fragment.
 */
class Fridge : Fragment(), View.OnClickListener {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private val adapter by lazy { FridgeViewPageAdapter(this) }

    private var frags: IFragments? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fridge, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        frags = requireActivity() as IFragments
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        boton_proteina.setOnClickListener(this)
        boton_frutas.setOnClickListener(this)
        boton_liquidos.setOnClickListener(this)
        boton_porcion.setOnClickListener(this)
        boton_salsas.setOnClickListener(this)
        boton_vegetales.setOnClickListener(this)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Fridge.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Fridge().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun onClick(v: View?) {
        v?.let{
            when(v.id){
                R.id.boton_proteina -> frags!!.replaceFragment(FridgeList(Category.FRIDGE, Category.Fridge.PROTEINS))
                R.id.boton_vegetales -> frags!!.replaceFragment(FridgeList(Category.FRIDGE, Category.Fridge.VEGIES))
                R.id.boton_frutas -> frags!!.replaceFragment(FridgeList(Category.FRIDGE, Category.Fridge.FRUITS))
                R.id.boton_salsas -> frags!!.replaceFragment(FridgeList(Category.FRIDGE, Category.Fridge.SAUCES))
                R.id.boton_porcion -> frags!!.replaceFragment(FridgeList(Category.FRIDGE, Category.Fridge.DISHES))
                R.id.boton_liquidos -> frags!!.replaceFragment(FridgeList(Category.FRIDGE, Category.Fridge.LIQUIDS))
            }
        }
    }
}