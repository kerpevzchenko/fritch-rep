package com.fcfm.fritch.fragments.recipes

import android.graphics.BitmapFactory
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import com.fcfm.fritch.MainActivity2
import com.fcfm.fritch.R
import com.fcfm.fritch.adapters.recycler_view.SelectIngredientsListAdapter
import com.fcfm.fritch.models.Ingredient
import com.fcfm.fritch.models.Recipe
import com.fcfm.fritch.models.User
import com.fcfm.fritch.utils.Category
import kotlinx.android.synthetic.main.fragment_add_recipe.*
import java.util.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Add_recipe.newInstance] factory method to
 * create an instance of this fragment.
 */
class AddRecipe : Fragment(), View.OnClickListener {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private var recipe: Recipe? = null
    private var ingredients: MutableList<Ingredient>? = null

    private var adapter : SelectIngredientsListAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_recipe, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ingredients = Ingredient.getAll(Category.ALL)

        /*ingredients = mutableListOf()
        ingredients!!.add(Ingredient.get(1)!!)
        ingredients!!.add(Ingredient.get(2)!!)
        ingredients!!.add(Ingredient.get(3)!!)*/

        ar_rcv.layoutManager = GridLayoutManager(requireContext(), 2)
        this.adapter = SelectIngredientsListAdapter(ingredients!!)
        ar_rcv.adapter = this.adapter

        val user = User(1, "Leonardo")

        val code = generateCode()

        recipe = Recipe(
            null,
            "Enchiladas",
            3,
            0,
            null,
            "Descr...",
            code,
            user
        )

        rAdd_btn.setOnClickListener(this)

        arImg_btn.setOnClickListener(this)
    }

    private fun generateCode():String{
        var code = List(4){
            (('A'..'Z') + ('0' .. '9')).random()
        }.joinToString("")

        return code
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Add_recipe.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            AddRecipe().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun onClick(v: View?) {
        v?.let { v ->
            when(v.id){
                R.id.rAdd_btn -> handleAddRecipeClick()
                R.id.arImg_btn -> (requireActivity() as MainActivity2).checkReadExternalStoragePermission()
            }
        }
    }

    private fun handleAddRecipeClick(){
        showProgressBarAtButton()
        val list = Ingredient.getCheckedFromList(ingredients!!)
        recipe!!.ingredients = list.toMutableList()

        recipe!!.addRecipe(
        {
            //handleSuccess(it)
        },
        {handleError(it)})
    }

    private fun handleSuccess(res: Int?){
        res?.let {
            var id: Int? = 0
            id = res

            if(id != -1){
                var success = true;
                recipe!!.id_recipe = id

                for(ingredient in recipe!!.ingredients!!){
                    recipe!!.addIngredientAPI(ingredient.id_ingredient!!,{},
                        {
                            success = false
                        })
                }

                recipe!!.online = true
                recipe!!.insert()
            }

        }

        hideProgressBarAtButton()
    }

    private fun handleError(t: Throwable){
        hideProgressBarAtButton()

        Toast.makeText(requireContext(), t.toString(), Toast.LENGTH_SHORT)
            .show()
    }

    private fun showProgressBarAtButton(){
        rAdd_pb.visibility = View.VISIBLE
        rAdd_btn.visibility = View.INVISIBLE
    }

    private fun hideProgressBarAtButton(){
        rAdd_pb.visibility = View.INVISIBLE
        rAdd_btn.visibility = View.VISIBLE
    }

    fun getImage(imgByte: ByteArray){
        val bitmap = BitmapFactory.decodeByteArray(imgByte, 0, imgByte.size)
        imgage.setImageBitmap(bitmap)
        recipe!!.img = Base64.getEncoder().encodeToString(imgByte)
    }
}