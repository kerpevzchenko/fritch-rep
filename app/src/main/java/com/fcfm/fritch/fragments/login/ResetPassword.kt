package com.fcfm.fritch.fragments.login

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.fcfm.fritch.MainActivity
import com.fcfm.fritch.R
import com.fcfm.fritch.interfaces.IFragments
import com.fcfm.fritch.models.Account
import kotlinx.android.synthetic.main.fragment_reset_password.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ResetPassword.newInstance] factory method to
 * create an instance of this fragment.
 */
class ResetPassword : Fragment(), View.OnClickListener {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    lateinit var frags: IFragments

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_reset_password, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rpReset_btn.setOnClickListener(this)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        frags = requireActivity() as MainActivity
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ResetPassword.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            ResetPassword().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun onClick(v: View?) {
        v?.let {
            when(v.id){
                R.id.rpReset_btn -> handleResetClick()
            }
        }
    }

    fun handleResetClick(){
        val email = rpEmail_txt.text.toString()
        val pass = rpPass_txt.text.toString()

        val account = Account(null, email, pass)

        showProgressBar()
        account.update(
            {
                hideProgressBar()
                handleSuccess(it!!)
            },
            {
                hideProgressBar()
                Toast.makeText(requireContext(), it.toString(), Toast.LENGTH_SHORT).show()
            }
        )
    }

    fun showProgressBar(){
        rpReset_btn.visibility = View.INVISIBLE
        rp_pb.visibility = View.VISIBLE
    }

    fun hideProgressBar(){
        rpReset_btn.visibility = View.VISIBLE
        rp_pb.visibility = View.INVISIBLE
    }

    fun handleSuccess(res: Int){
        if(res == 1){
            frags.prevFragment()
        }
        else{

        }
    }
}