package com.fcfm.fritch.fragments.recipes

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.fcfm.fritch.MainActivity2
import com.fcfm.fritch.R
import com.fcfm.fritch.models.Recipe
import kotlinx.android.synthetic.main.fragment_view_instructions.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_ID = "id"
private const val ARG_DOWNLOADED = "downloaded"

/**
 * A simple [Fragment] subclass.
 * Use the [View_instructions.newInstance] factory method to
 * create an instance of this fragment.
 */
class View_instructions : Fragment() {
    // TODO: Rename and change types of parameters
    private var id_recipe: Int = 0
    private var downloaded: Boolean = true

    private var recipe : Recipe? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            id_recipe = it.getInt(ARG_ID)
            downloaded = it.getBoolean(ARG_DOWNLOADED)
        }

        if(downloaded){
            recipe = Recipe.getRecipe(id_recipe)
        }
        else{
            Recipe.getRecipe(id_recipe,
                { handleSuccess(it!!) },
                { handleError(it) })
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_view_instructions, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if(downloaded)
        vd_txt.text = recipe!!.descr
    }

    private fun handleSuccess(recipe: Recipe)
    {
        vd_txt.text = recipe.descr
    }

    private fun handleError(t: Throwable)
    {
        (requireActivity() as MainActivity2).tostError(t)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment View_instructions.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(id: Int, downloaded: Boolean) =
            View_instructions().apply {
                arguments = Bundle().apply {
                    putInt(ARG_ID, id)
                    putBoolean(ARG_DOWNLOADED, downloaded)
                }
            }
    }
}