package com.fcfm.fritch.fragments.login

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.fcfm.fritch.MainActivity
import com.fcfm.fritch.R
import com.fcfm.fritch.api.AccountAPI
import com.fcfm.fritch.interfaces.IFragments
import com.fcfm.fritch.models.Account
import kotlinx.android.synthetic.main.fragment_signin.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Signin.newInstance] factory method to
 * create an instance of this fragment.
 */
class Signin : Fragment(), View.OnClickListener {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private var account: Account? = null

    lateinit var frags: IFragments

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_signin, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        si_btn.setOnClickListener(this)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        frags = requireActivity() as MainActivity
    }

    override fun onClick(v: View?) {
        v?.let {
            when(v.id){
                R.id.si_btn -> handleSignInClick()
            }
        }
    }

    private fun showProgressBar(){
        si_btn.visibility = View.INVISIBLE
        si_pb.visibility = View.VISIBLE
    }

    private fun hideProgressBar(){
        si_btn.visibility = View.VISIBLE
        si_pb.visibility = View.INVISIBLE
    }

    private fun handleSignInClick(){
        if(siEmail_txt.text.toString().isEmpty() || siPass_txt.text.toString().isEmpty()){
            Toast.makeText(requireContext(), R.string.empty_field, Toast.LENGTH_SHORT).show()
        }
        else{
            showProgressBar()
            account = Account(null, siEmail_txt.text.toString(), siPass_txt.text.toString())
            account!!.signup(
                {
                    hideProgressBar()
                    handleSuccess(it!!)
                },
                {
                    handleError(it)
                    hideProgressBar()
                })
        }

    }

    private fun handleSuccess(id: Int){
        when(id){
            0 -> Toast.makeText(requireContext(), "Error", Toast.LENGTH_SHORT).show()
            23000 -> Toast.makeText(requireContext(), "Email already on use", Toast.LENGTH_SHORT).show()
            else -> {
                Toast.makeText(requireContext(), "Sign Up Successfully", Toast.LENGTH_SHORT).show()
                frags.prevFragment()
            }
        }
    }

    private fun handleError(t: Throwable){
        (requireContext() as MainActivity).tostError(t)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Signin.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Signin().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }


}