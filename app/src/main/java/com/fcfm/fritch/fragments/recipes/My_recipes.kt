package com.fcfm.fritch.fragments.recipes

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import com.fcfm.fritch.MainActivity2
import com.fcfm.fritch.R
import com.fcfm.fritch.UserApplication.Companion.pref
import com.fcfm.fritch.adapters.recycler_view.RecipesItemListAdapter
import com.fcfm.fritch.api.RecipeAPI
import com.fcfm.fritch.interfaces.IFragments
import com.fcfm.fritch.interfaces.IRVOnClick
import com.fcfm.fritch.models.Ingredient
import com.fcfm.fritch.models.Recipe
import kotlinx.android.synthetic.main.fragment_my_recipes.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [My_recipes.newInstance] factory method to
 * create an instance of this fragment.
 */
class My_recipes : Fragment(), IRVOnClick.Recipe, View.OnClickListener {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private var adapter : RecipesItemListAdapter? = null

    private var recipesArray = mutableListOf<Recipe>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_recipes, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rmr_rcv.layoutManager = GridLayoutManager(requireContext(),2)
        recipesArray = Recipe.getMyRecipes(pref.getInt(pref.USER))
        val savedRecipes = Recipe.getSavedRecipes(pref.getInt(pref.USER))
        for (item in savedRecipes){
            recipesArray.add(item)
        }

        this.adapter = RecipesItemListAdapter(this.requireContext(), recipesArray, this)
        rmr_rcv.adapter = this.adapter
        mrAdd_btn.setOnClickListener(this)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment My_recipes.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            My_recipes().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    /*Click a los elementos del fragment
    * */
    override fun onClick(v: View?) {
        v?.let {
            when(v.id){
                R.id.mrAdd_btn -> {
                    val frag = EditRecipe.newInstance(EditRecipe.ADD, 0)
                    (requireContext() as MainActivity2).replaceFragment(frag)
                }
            }
        }
    }

    /*Click a los elementos del recycler view
    * */
    override fun onClick(recipe: Recipe) {
        if (recipe.user!!.id_user == pref.getInt(pref.USER)){
            val frag = EditRecipe.newInstance(EditRecipe.EDIT, recipe.id_recipe!!)
            (requireContext() as MainActivity2).replaceFragment(frag)
        }
        else{
            val frag = View_recipes.newInstance(recipe.id_recipe!!, true)
            (requireContext() as MainActivity2).replaceFragment(frag)
        }
    }
}