package com.fcfm.fritch.fragments.recipes

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.fcfm.fritch.R
import com.fcfm.fritch.adapters.ViewRecipeViewPageAdapter
import com.fcfm.fritch.models.Recipe
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.fragment_view_recipes.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_ID = "id"
private const val ARG_DOWNLOADED = "downloaded"

/**
 * A simple [Fragment] subclass.
 * Use the [View_recipes.newInstance] factory method to
 * create an instance of this fragment.
 */
class View_recipes : Fragment() {
    // TODO: Rename and change types of parameters
    private var id_recipe: Int = 0
    private var downloaded: Boolean = true

    //private val adapter by lazy{ ViewRecipeViewPageAdapter(this, id) }
    private var adapter: ViewRecipeViewPageAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            id_recipe = it.getInt(ARG_ID)
            downloaded = it.getBoolean(ARG_DOWNLOADED)
        }

        adapter = ViewRecipeViewPageAdapter(this, id_recipe, downloaded)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_view_recipes, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        R_pager.adapter = adapter

        val tabLayoutMediator = TabLayoutMediator(R_tabLayout, R_pager,
            TabLayoutMediator.TabConfigurationStrategy{ tab, position ->
                when(position){
                    0 -> {
                        tab.text = "Ingredients"
                    }
                    1 -> {
                        tab.text = "Instructions"
                    }
                }
            })

        tabLayoutMediator.attach()
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment View_recipes.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(id: Int, downloaded: Boolean) =
            View_recipes().apply {
                arguments = Bundle().apply {
                    putInt(ARG_ID,id)
                    putBoolean(ARG_DOWNLOADED, downloaded)
                }
            }
    }
}