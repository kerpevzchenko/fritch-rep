package com.fcfm.fritch.fragments

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.fcfm.fritch.MainActivity2
import com.fcfm.fritch.R
import com.fcfm.fritch.UserApplication.Companion.dbHelper
import com.fcfm.fritch.UserApplication.Companion.pref
import com.fcfm.fritch.adapters.SeasoningListAdapter
import com.fcfm.fritch.adapters.recycler_view.IngredientsListAdapter
import com.fcfm.fritch.adapters.recycler_view.SearchIngredientAdapter
import com.fcfm.fritch.api.RestEngine
import com.fcfm.fritch.dialog.AddIngredientDialog
import com.fcfm.fritch.interfaces.IDialog
import com.fcfm.fritch.interfaces.IFragments
import com.fcfm.fritch.interfaces.IRVOnClick
import com.fcfm.fritch.interfaces.IService
import com.fcfm.fritch.models.DataDbHelper
import com.fcfm.fritch.models.Ingredient
import com.fcfm.fritch.utils.Category
import kotlinx.android.synthetic.main.fragment_add_ingredient.*
import kotlinx.android.synthetic.main.fragment_seasonings.*
import kotlinx.android.synthetic.main.item_ingredient.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Add_ingredient.newInstance] factory method to
 * create an instance of this fragment.
 */
class Add_ingredient(val category: Int, val type: Int? = null) : Fragment(),
    IRVOnClick.Ingredient, View.OnClickListener, SearchView.OnQueryTextListener, IDialog.Ingredient
{
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private var adapter : SearchIngredientAdapter? = null
    private val ingredientArray = mutableListOf<Ingredient>()

    private var frag : IFragments? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_ingredient, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        frag = (requireActivity() as MainActivity2)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ai_rcv.layoutManager = LinearLayoutManager(this.requireContext())
        this.adapter = SearchIngredientAdapter(this.requireContext(), ingredientArray, this, category)
        ai_rcv.adapter = this.adapter

        //ai_btn.setOnClickListener(this)

        aiSearch_sv.setOnQueryTextListener(this)
    }

    private fun handleAddButtonClick(){
        /*val category = chooseFritch_spinner.selectedItem.toString()
        Toast.makeText(requireContext(), category, Toast.LENGTH_SHORT).show()*/
        /*when(category){
            Category.SEASONING -> dbHelper.myIngredients.insert()

        }*/
    }

    /*companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Add_ingredient.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Add_ingredient().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }*/

    //Click listener de los item en el recycler view
    override fun onClick(ingredient: Ingredient) {
        if(category != Category.SEASONING){
            Toast.makeText(requireContext(), ingredient.name, Toast.LENGTH_SHORT).show()

            val dialog:AddIngredientDialog = AddIngredientDialog(this, ingredient)
            dialog.show(requireActivity().supportFragmentManager, "AddIngr")
        }
        else{
            getIngredient(ingredient)
        }
    }

    //Click listener de los view en el fragmento
    override fun onClick(v: View?) {
        when(v!!.id){
            //R.id.ai_btn -> handleAddButtonClick()
        }
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        val search = Ingredient.Search(query, pref.getInt(pref.ACCOUNT))
        Ingredient.searchAPI(search,
            { setIngredientArray(it) },
            { (requireActivity() as MainActivity2).tostError(it) }
        )

        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        val search = Ingredient.Search(newText, pref.getInt(pref.ACCOUNT))
        Ingredient.searchAPI(search!!,
            { setIngredientArray(it) },
            { (requireActivity() as MainActivity2).tostError(it) }
        )

        return true
    }


    override fun getIngredient(ingredient: Ingredient) {
        //Toast.makeText(requireContext(), ingredient.amount.toString() + ingredient.unit, Toast.LENGTH_SHORT).show()

        ingredient.id_account = pref.getInt(pref.ACCOUNT)
        if(ingredient.amount == null) ingredient.amount = 0
        //ingredient.online = true
        ingredient.category = category
        if(category == Category.FRIDGE) ingredient.type = type

        if(ingredient.insert()){
            ingredient.save(
                {
                    if (it == 1){
                        ingredient.online = true
                        ingredient.updateOnline()
                        frag!!.prevFragment()
                    }
                },
                {
                    Log.e("ErrorNetwork addIngredient", it.toString())
                })
        }
        else{
            Log.e("Error SQLITE","AddIngredient insert error")
        }
    }

    private fun setIngredientArray(items: List<Ingredient>?){
        items?.let{
            ingredientArray.clear()

            for(item in items){
                ingredientArray.add(item)
            }

            adapter!!.notifyDataSetChanged()
        }

    }


}