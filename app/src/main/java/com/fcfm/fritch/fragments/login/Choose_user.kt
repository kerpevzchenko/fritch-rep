package com.fcfm.fritch.fragments.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import com.fcfm.fritch.MainActivity
import com.fcfm.fritch.MainActivity2
import com.fcfm.fritch.R
import com.fcfm.fritch.UserApplication.Companion.pref
import com.fcfm.fritch.adapters.recycler_view.ItemUserListAdapter
import com.fcfm.fritch.api.AccountAPI
import com.fcfm.fritch.api.UserAPI
import com.fcfm.fritch.dialog.AddIngredientDialog
import com.fcfm.fritch.dialog.CreateUserDialog
import com.fcfm.fritch.fragments.recipes.Recipes
import com.fcfm.fritch.interfaces.IDialog
import com.fcfm.fritch.interfaces.IFragments
import com.fcfm.fritch.interfaces.IRVOnClick
import com.fcfm.fritch.models.User
import kotlinx.android.synthetic.main.fragment_choose_user.*
import java.lang.Exception

private const val FROM = "from"



/**
 * A simple [Fragment] subclass.
 * Use the [Choose_user.newInstance] factory method to
 * create an instance of this fragment.
 */
class Choose_user : Fragment(), IRVOnClick.User, View.OnClickListener, IDialog.User {
    private var from: Int? = null
    private var adapter: ItemUserListAdapter? = null
    private val users = mutableListOf<User>()
    private var frag : IFragments? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            from = it.getInt(FROM)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_choose_user, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        val activity = requireActivity()
        if(activity is MainActivity){
            frag = activity
        } else if (activity is MainActivity2){
            frag = activity
        }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        chu_rcv.layoutManager = GridLayoutManager(requireContext(), 2)
        this.adapter = ItemUserListAdapter(requireContext(), users, this)
        chu_rcv.adapter = adapter

        chuNew_lbl.setOnClickListener(this)

        AccountAPI.getUsers(pref.getInt(pref.ACCOUNT),
            {
                handleSuccess(it!!)
            },
            {
                handleError(it)
            })
    }

    private fun handleSuccess(list: List<User>){
        users.clear()

        for (item in list){
            users.add(item)
        }
        adapter!!.notifyDataSetChanged()
    }

    private fun handleError(t: Throwable){
        (requireContext() as MainActivity).tostError(t)
    }

    companion object {
        const val LOGIN = 1
        const val ACCOUNT = 2
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Choose_user.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(from: Int) =
            Choose_user().apply {
                arguments = Bundle().apply {
                    putInt(FROM, from)
                }
            }
    }

    //Click a un elemento dentro del recycler view
    override fun onClick(user: User) {
        pref.save(pref.USER, user.id_user)
        pref.save(pref.USERNAME, user.name)

        when(from){
            Choose_user.LOGIN -> {
                val intent = Intent(requireContext(), MainActivity2::class.java)
                startActivity(intent)
            }
            Choose_user.ACCOUNT -> {
                frag!!.clearStack()
                frag!!.replaceFragment(Recipes())
            }
        }

    }

    //Click a un elemento dentro del fragment
    override fun onClick(v: View?) {
        v?.let {
            when(v.id){
                R.id.chuNew_lbl -> {
                    handleNewUserClick()
                }
            }
        }
    }

    private fun handleNewUserClick(){
        val dialog: CreateUserDialog = CreateUserDialog(this, pref.getInt(pref.ACCOUNT))
        dialog.isCancelable = false
        dialog.show(requireActivity().supportFragmentManager, "CreateUser")
    }

    override fun getUser(user: User) {

        user.id_user?.let {
            users.add(user)
            adapter!!.notifyDataSetChanged()
        } ?.run {

        }

    }
}