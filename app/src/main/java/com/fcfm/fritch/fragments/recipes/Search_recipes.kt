package com.fcfm.fritch.fragments.recipes

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.SearchView
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import com.fcfm.fritch.MainActivity2
import com.fcfm.fritch.R
import com.fcfm.fritch.UserApplication.Companion.pref
import com.fcfm.fritch.adapters.recycler_view.RecipesItemListAdapter
import com.fcfm.fritch.dialog.EnterTextDialog
import com.fcfm.fritch.interfaces.IDialog
import com.fcfm.fritch.interfaces.IRVOnClick
import com.fcfm.fritch.models.Recipe
import kotlinx.android.synthetic.main.fragment_search_recipes.*
import java.time.LocalDate
import java.time.format.DateTimeFormatter

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"
private const val ARG_INGREDIENT = "ingredient"

/**
 * A simple [Fragment] subclass.
 * Use the [Search_recipes.newInstance] factory method to
 * create an instance of this fragment.
 */
class Search_recipes : Fragment(), IRVOnClick.Recipe, IDialog.Text, View.OnClickListener, SearchView.OnQueryTextListener, AdapterView.OnItemSelectedListener {
    // TODO: Rename and change types of parameters
    private var ingredient: Int = 0

    private var adapter : RecipesItemListAdapter? = null

    private val recipesArray = mutableListOf<Recipe>()
    private var recipes = mutableListOf<Recipe>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            ingredient = it.getInt(ARG_INGREDIENT, 0)
        }

        if (ingredient != 0){
            Recipe.getByIngredient(ingredient!!,
                {
                    recipesArray.clear()
                    if (it != null) {
                        for (item in it){
                            recipes.add(item)
                        }
                        adapter?.notifyDataSetChanged()
                    }
                    else{
                        Toast.makeText(requireContext(), R.string.no_data, Toast.LENGTH_SHORT).show()
                    }
                },
                {
                    handleError(it)
                })
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search_recipes, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rsCode_btn.setOnClickListener(this)

        rsSearch_sv.setOnQueryTextListener(this)

        rsFilter_spin.onItemSelectedListener = this

        rs_rcv.layoutManager = GridLayoutManager(requireContext(), 2)
        this.adapter = RecipesItemListAdapter(this.requireContext(), recipes, this)
        rs_rcv.adapter = this.adapter

        if(ingredient == 0)
            Recipe.getRecipes(
                { handleSuccess(it!!) },
                { handleError(it) })
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Search_recipes.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(ingredient: Int) =
            Search_recipes().apply {
                arguments = Bundle().apply {
                    this.putInt(ARG_INGREDIENT, ingredient)
                }
            }
    }

    override fun onClick(recipe: Recipe) {
        var aux = false
        if(recipe.user!!.id_user == pref.getInt(pref.USER) ||
            recipe.alreadySaved(pref.getInt(pref.USER))) aux = true

        val frag = View_recipes.newInstance(recipe.id_recipe!!, aux)
        (requireContext() as MainActivity2).replaceFragment(frag)
    }

    fun handleSuccess(list: List<Recipe>){
        recipesArray.clear()
        for(recipe in list){
            recipesArray.add(recipe)
            recipes.add(recipe)
        }
        adapter!!.notifyDataSetChanged()
    }

    fun handleError(t: Throwable){
        try {
            val act = requireActivity() as MainActivity2?

            act?.tostError(t)
        }
        catch (e:Exception){

        }

    }

    override fun getText(text: String) {
        if(text.length == 0){
            Toast.makeText(requireContext(), R.string.empty_field, Toast.LENGTH_SHORT).show()
        }
        else if(text.length > 4){
            Toast.makeText(requireContext(), "Max lenght is 4", Toast.LENGTH_SHORT).show()
        }
        else{
            Recipe.getRecipe(text,
                {
                    if ( it!!.name != null){
                        val recipe = it

                        var aux = false
                        if(recipe!!.user!!.id_user == pref.getInt(pref.USER) ||
                            recipe!!.alreadySaved(pref.getInt(pref.USER))) aux = true

                        val frag = View_recipes.newInstance(recipe.id_recipe!!, aux)
                        (requireContext() as MainActivity2).replaceFragment(frag)
                    }
                    else{
                        Toast.makeText(requireActivity(), R.string.not_found, Toast.LENGTH_SHORT).show()
                    }

                },
                {
                    Toast.makeText(requireActivity(), R.string.not_found, Toast.LENGTH_SHORT).show()
                })
        }

    }

    override fun onClick(v: View?) {
        v?.let{
            when(v.id){
                R.id.rsCode_btn -> handleSearchCodeClick()
            }
        }
    }

    private fun handleSearchCodeClick(){
        val dialog = EnterTextDialog(this)
        dialog.show(requireActivity().supportFragmentManager, "searchCode")
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        TODO("Not yet implemented")
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        val filter = recipesArray.filter {
            it.name!!.contains(newText.toString(),false)
        }

        recipes.clear()
        for(item in filter){
            recipes.add(item)
        }
        adapter!!.notifyDataSetChanged()
        return true
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        parent?.let {
            when(parent.id){
                R.id.rsFilter_spin -> {
                    when(rsFilter_spin.selectedItemPosition){
                        0 -> { // New
                            val filter = recipesArray
                            filter.sortByDescending {
                                LocalDate.parse(it.date, DateTimeFormatter.ISO_DATE)
                            }

                            recipes.clear()
                            for(item in filter){
                                recipes.add(item)
                            }
                            adapter!!.notifyDataSetChanged()
                        }
                        1 -> { // Most Downloaded
                            val filter = recipesArray
                            filter.sortByDescending {
                                it.downloads
                            }

                            recipes.clear()
                            for(item in filter){
                                recipes.add(item)
                            }
                            adapter!!.notifyDataSetChanged()
                        }
                    }
                }
            }
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        TODO("Not yet implemented")
    }
}