package com.fcfm.fritch.fragments.login

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.fcfm.fritch.MainActivity
import com.fcfm.fritch.R
import com.fcfm.fritch.UserApplication.Companion.pref
import com.fcfm.fritch.api.AccountAPI
import com.fcfm.fritch.api.RestEngine
import com.fcfm.fritch.interfaces.IFragments
import com.fcfm.fritch.interfaces.IService
import com.fcfm.fritch.models.Account
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.android.synthetic.main.fragment_signin.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Login.newInstance] factory method to
 * create an instance of this fragment.
 */
class Login : Fragment(), View.OnClickListener {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    var account: Account? = null

    private var frag: IFragments? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        frag = requireActivity() as MainActivity
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        l_btn.setOnClickListener(this)
        lForPass_lbl.setOnClickListener(this)
        lCrAcc_lbl.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        v?.let{
            when(v.id){
                R.id.l_btn -> handleLoginClick()
                R.id.lForPass_lbl -> frag!!.replaceFragment(ResetPassword())
                R.id.lCrAcc_lbl -> frag!!.replaceFragment(Signin())
            }
        }
    }

    private fun handleLoginClick(){
        if(lEmail_txt.text.toString().isEmpty() || lPass_txt.text.toString().isEmpty()){
            Toast.makeText(requireContext(), R.string.empty_field, Toast.LENGTH_SHORT).show()
        }
        else{
            account = Account(null, lEmail_txt.text.toString(), lPass_txt.text.toString())
            showProgressBar()
            account!!.login(
                {  hideProgressBar(); handleSuccess(it!!); },
                {  hideProgressBar(); handleError(it); })
        }

    }

    private fun handleSuccess(id: Int){
        if(id != 0){
            account!!.id_account = id

            pref.save<Int>(pref.ACCOUNT, id)
            pref.save(pref.EMAIL, account!!.email)

            val frag = Choose_user.newInstance(Choose_user.LOGIN)
            (requireActivity() as MainActivity).replaceFragment(frag)
        }
        else{
            Toast.makeText(requireActivity(), R.string.wrong_cred, Toast.LENGTH_SHORT).show()
        }


    }

    private fun handleError(t: Throwable){
        (requireActivity() as MainActivity).tostError(t)
    }

    private fun showProgressBar(){
        l_pb.visibility = View.VISIBLE
        l_btn.visibility = View.INVISIBLE
    }

    private fun hideProgressBar(){
        l_pb.visibility = View.INVISIBLE
        l_btn.visibility = View.VISIBLE
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Login.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Login().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}