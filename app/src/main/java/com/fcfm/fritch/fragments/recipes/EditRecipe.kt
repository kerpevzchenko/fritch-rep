package com.fcfm.fritch.fragments.recipes

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.graphics.BitmapFactory
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.fcfm.fritch.MainActivity2
import com.fcfm.fritch.R
import com.fcfm.fritch.UserApplication.Companion.pref
import com.fcfm.fritch.adapters.recycler_view.SelectIngredientsListAdapter
import com.fcfm.fritch.interfaces.IFragments
import com.fcfm.fritch.models.Ingredient
import com.fcfm.fritch.models.Recipe
import com.fcfm.fritch.models.User
import com.fcfm.fritch.utils.Category
import kotlinx.android.synthetic.main.fragment_edit_recipe.*
import java.util.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val MODE = "mode"
private const val ID = "id"

/**
 * A simple [Fragment] subclass.
 * Use the [EditRecipe.newInstance] factory method to
 * create an instance of this fragment.
 */
class EditRecipe : Fragment(), View.OnClickListener {
    // TODO: Rename and change types of parameters
    private var id_recipe: Int = 0
    private var mode: Int? = null

    private var ingredients = mutableListOf<Ingredient>()
    private var adapter: SelectIngredientsListAdapter? = null

    private var recipe : Recipe? = null

    private var frag : IFragments? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            mode = it.getInt(MODE)
            id_recipe = it.getInt(ID)
        }

        if(id_recipe != 0){
            recipe = Recipe.getRecipe(id_recipe)
            ingredients.clear()
            for(item in recipe!!.ingredients!!){
                ingredients.add(item)
            }
        }
        else{
            recipe = Recipe()
            recipe!!.portions = 0

            ingredients = Ingredient.getAll(Category.ALL)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_edit_recipe, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        frag = (requireActivity() as MainActivity2)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        erDone_btn.setOnClickListener(this)
        erUploadImg_btn.setOnClickListener(this)
        erInc_btn.setOnClickListener(this)
        erDec_btn.setOnClickListener(this)
        erDel_btn.setOnClickListener(this)

        er_rcv.layoutManager = LinearLayoutManager(requireContext())
        this.adapter = SelectIngredientsListAdapter(ingredients, true)
        er_rcv.adapter = this.adapter

        if(recipe!!.id_recipe != null) setDataToView()

        if(mode == EDIT){
            delOptions.visibility = View.VISIBLE
        }

        if(mode == ADD) erCode_txt.visibility = View.INVISIBLE
    }

    private fun setDataToView(){

        erName_txt.setText(recipe!!.name)
        erPortions_txt.text = recipe!!.portions.toString()
        erType_spin.setSelection(recipe!!.type!!)
        erCode_txt.text = recipe!!.code
        erDescr_txt.setText(recipe!!.descr)

        val imgByte = Base64.getDecoder().decode(recipe!!.img)
        val bitmap = BitmapFactory.decodeByteArray(imgByte, 0, imgByte.size)
        erImg_iv.setImageBitmap(bitmap)

        ingredients.clear()
        for(item in recipe!!.ingredients!!){
            ingredients.add(item)
        }
        if( ingredients.size > 0 ) this.adapter!!.notifyDataSetChanged()


    }

    private fun getDataFromView(){
        recipe!!.name = erName_txt.text.toString()
        recipe!!.descr = erDescr_txt.text.toString()
        recipe!!.portions = erPortions_txt.text.toString().toInt()
        recipe!!.type = erType_spin.selectedItemPosition
        recipe!!.online = false

        if(mode == ADD){
            val list = Ingredient.getCheckedFromList(ingredients!!)
            recipe!!.ingredients = list.toMutableList()
        }

        recipe!!.code = generateCode()

        recipe!!.user = User(pref.getInt(pref.USER), pref.getString(pref.USERNAME))
    }

    companion object {
        const val ADD = 1
        const val EDIT = 2

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param mode Mode between [ADD] or [EDIT].
         * @return A new instance of fragment EditRecipe.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(mode: Int, id: Int) =
            EditRecipe().apply {
                arguments = Bundle().apply {
                    putInt(MODE, mode)
                    putInt(ID, id)
                }
            }
    }

    override fun onClick(v: View?) {
        v?.let {
            when(v.id){
                R.id.erDone_btn -> handleDoneClick()
                R.id.erUploadImg_btn -> (requireActivity() as MainActivity2).checkReadExternalStoragePermission()
                R.id.erInc_btn -> handleAddPortions(1)
                R.id.erDec_btn -> handleAddPortions(-1)
                R.id.erDel_btn -> handleDeleteClick()
            }
        }
    }

    private fun handleDeleteClick() {
        val dialog = AlertDialog.Builder(requireContext())
        dialog.setMessage(R.string.want_to_delete).
        setPositiveButton(R.string.yes, DialogInterface.OnClickListener{
                dialog, which ->
            showProgressBar2()

            val rec = Recipe(id_recipe = recipe!!.id_recipe)

            rec.delete(
            {
                if(it == 1){
                    if(recipe!!.delete(false)){
                        hideProgressBar2()
                        frag!!.prevFragment()
                    }
                    else{
                        hideProgressBar2()
                    }
                }
                else{
                    hideProgressBar2()
                }
            },
            {
                Toast.makeText(requireContext(), it.toString(), Toast.LENGTH_SHORT).show()
                hideProgressBar2()
            })

        }).
        setNegativeButton(R.string.no, DialogInterface.OnClickListener{
                dialog, which ->
        })

        dialog.show()
    }

    private fun handleDoneClick() {
        getDataFromView()

        if(recipe!!.name!!.isEmpty() ||
            recipe!!.descr!!.isEmpty() ||
            recipe!!.img == null ||
            recipe!!.ingredients!!.size == 0){
            Toast.makeText(requireActivity(), R.string.empty_field, Toast.LENGTH_SHORT).show()
        }
        else if(recipe!!.name!!.length > 30){
            Toast.makeText(requireContext(), "Max lenght is 30", Toast.LENGTH_SHORT).show()
        }
        else {
            showProgressBar()
            when(mode){
                ADD -> {
                    recipe!!.addRecipe(
                        {
                            recipe!!.id_recipe = it!!.id_recipe
                            if(recipe!!.insert()){
                                handleAddSuccess(it)
                            }
                            hideProgressBar()
                            frag!!.prevFragment()

                        },
                        {
                            handleError(it)
                            hideProgressBar()
                        }
                    )

                }

                EDIT -> {
                    if(recipe!!.update()){
                        recipe!!.update(
                            {
                                handleEditSuccess(it)
                                hideProgressBar()
                                frag!!.prevFragment()
                            },
                            {
                                handleError(it)
                                hideProgressBar()
                            })
                    }
                }
            }
        }



    }

    private fun handleEditSuccess(last_update: String?) {
        recipe!!.last_update = last_update
        recipe!!.updateLastUpdate()

        recipe!!.online = true
        recipe!!.updateOnline()

        hideProgressBar()
    }

    private fun generateCode():String{
        val code = List(4){
            (('A'..'Z') + ('0' .. '9')).random()
        }.joinToString("")

        return code
    }

    private fun handleAddSuccess(res: Recipe?){
        res?.let {
            var id: Int? = 0
            id = res.id_recipe

            if(id != -1){
                var success = true;
                recipe!!.id_recipe = id

                for(ingredient in recipe!!.ingredients!!){
                    recipe!!.addIngredientAPI(ingredient.id_ingredient!!,{},
                        {
                            success = false
                        })
                }

                recipe!!.online = true
                recipe!!.date = res.last_update
                recipe!!.last_update = res.last_update

                recipe!!.updateLastUpdate()
                recipe!!.updateOnline()
                recipe!!.updateDate()


            }

        }
        hideProgressBar()
    }

    private fun handleError(t: Throwable){
        hideProgressBar()

        Toast.makeText(requireContext(), t.toString(), Toast.LENGTH_SHORT)
            .show()
    }

    fun getImage(imgByte: ByteArray){
        val bitmap = BitmapFactory.decodeByteArray(imgByte, 0, imgByte.size)
        erImg_iv.setImageBitmap(bitmap)
        recipe!!.img = Base64.getEncoder().encodeToString(imgByte)
    }

    private fun showProgressBar(){
        er_pb.visibility = View.VISIBLE
        erDone_btn.visibility = View.INVISIBLE
    }

    private fun hideProgressBar(){
        er_pb.visibility = View.INVISIBLE
        erDone_btn.visibility = View.VISIBLE
    }

    private fun showProgressBar2(){
        er_pb2.visibility = View.VISIBLE
        erDel_btn.visibility = View.INVISIBLE
    }

    private fun hideProgressBar2(){
        er_pb2.visibility = View.INVISIBLE
        erDel_btn.visibility = View.VISIBLE
    }

    private fun handleAddPortions(value: Int){
        var x = recipe!!.portions!!

        x += value

        if(x < 1){
            x = 1
        }

        recipe!!.portions = x
        erPortions_txt.text = recipe!!.portions.toString()
    }
}