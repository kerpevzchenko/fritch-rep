package com.fcfm.fritch.fragments

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.fcfm.fritch.MainActivity2
import com.fcfm.fritch.R
import com.fcfm.fritch.UserApplication.Companion.dbHelper
import com.fcfm.fritch.UserApplication.Companion.pref
import com.fcfm.fritch.adapters.SeasoningListAdapter
import com.fcfm.fritch.api.IngredientAPI
import com.fcfm.fritch.api.RestEngine
import com.fcfm.fritch.interfaces.IFragments
import com.fcfm.fritch.interfaces.IRVOnClick
import com.fcfm.fritch.interfaces.IService
import com.fcfm.fritch.models.Ingredient
import com.fcfm.fritch.utils.Category
import kotlinx.android.synthetic.main.fragment_seasonings.*
import kotlinx.android.synthetic.main.seasoning_item.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Seasonings.newInstance] factory method to
 * create an instance of this fragment.
 */
class Seasonings() : Fragment(), IRVOnClick.Ingredient, View.OnClickListener, SearchView.OnQueryTextListener  {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private var adapter: SeasoningListAdapter? = null

    private var seasoningArray = mutableListOf<Ingredient>()

    private var frags: IFragments? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
        //initializeSeasoningArray()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_seasonings, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        frags = requireActivity() as MainActivity2
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        seasoningArray = Ingredient.getAll(Category.SEASONING)

        //RecyclerView de los condimentos
        s_rcv.layoutManager = LinearLayoutManager(this.requireContext())
        this.adapter = SeasoningListAdapter(this.requireContext(), seasoningArray, this)
        s_rcv.adapter = this.adapter

        sAdd_btn.setOnClickListener(this)

    }

    /*companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Seasonings.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Seasonings().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }*/

    //Click en los elemento del recyclerView
    override fun onClick(ingredient: Ingredient) {
        val frag = ViewIngredient.newInstance(ingredient.id_ingredient!!)
        frags!!.replaceFragment(frag)
    }

    //Click en los elementos del fragment
    override fun onClick(v: View?) {
        v?.let{
            when(it.id)
            {
                R.id.sAdd_btn -> {
                    (requireActivity() as IFragments).replaceFragment(Add_ingredient(Category.SEASONING))
                }

            }
        }
    }

    override fun onStop() {
        super.onStop()

        for(item in seasoningArray){
            if(!item.online){
                item.id_account = pref.getInt(pref.ACCOUNT)
                if(item.update()){
                    item.update(
                        {
                            if(it == 1){
                                item.online = true
                                item.updateOnline()
                            }
                        },
                        {
                            Log.e("SeasoningUpdateError", it.toString())
                        })
                }
            }
        }
    }



    override fun onQueryTextSubmit(query: String?): Boolean {


        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {


        return true
    }
}