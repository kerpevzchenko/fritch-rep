package com.fcfm.fritch.utils

import com.fcfm.fritch.models.Ingredient

class SetDB {
    companion object{
        val DB_NAME = "dbFritch"
        val DB_VERSION = 1
    }

    abstract class TblAccount{

    }

    abstract class TblUser{
        companion object{
            val TABLE_NAME = "User"
            val COL_ID = "id_user"
            val COL_NAME = "name"

            val createTableStr = "CREATE TABLE " + TABLE_NAME + "(" +
                    COL_ID + " INTEGER PRIMARY KEY," +
                    COL_NAME + " TEXT)"
        }

    }

    abstract class TblMyIngredient{
        companion object {
            val TABLE_NAME = "MyIngredient"
            val COL_ID = "id_ingredient"
            val COL_NAME = "name"
            val COL_AMOUNT = "amount"
            val COL_UNIT = "unit"
            val COL_CATEGORY = "category"
            val COL_TYPE = "type"
            val COL_ONLINE = "online"

            val createTableStr = "CREATE TABLE " + TABLE_NAME + "(" +
                    COL_ID + " INTEGER PRIMARY KEY," +
                    COL_NAME + " TEXT," +
                    COL_AMOUNT + " INTEGER," +
                    COL_UNIT + " INTEGER," +
                    COL_CATEGORY + " INTEGER," +
                    COL_TYPE + " INTEGER," +
                    COL_ONLINE + " INTEGER)"
        }
    }

    abstract class TblRecipe{
        companion object{
            val TABLE_NAME = "Recipe"
            val COL_ID = "id_recipe"
            val COL_NAME = "name"
            val COL_DESCR = "descr"
            val COL_PORTIONS = "portions"
            val COL_TYPE = "type"
            val COL_IMG = "img"
            val COL_ID_USER = "id_user"
            val COL_USER_NAME = "user_name"
            val COL_CODE = "code"
            val COL_ONLINE = "online"
            val COL_LAST_UPDATE = "last_update"
            val COL_DATE = "date"

            val createTableStr = " CREATE TABLE " + TABLE_NAME + "(" +
                    COL_ID + " INTEGER PRIMARY KEY," +
                    COL_NAME + " TEXT," +
                    COL_DESCR + " TEXT," +
                    COL_PORTIONS + " INTEGER," +
                    COL_TYPE + " INTEGER," +
                    COL_IMG + " BLOB," +
                    COL_DATE + " TEXT," +
                    COL_LAST_UPDATE + " TEXT," +
                    COL_ID_USER + " INT," +
                    COL_USER_NAME + " TEXT," +
                    COL_CODE + " TEXT," +
                    COL_ONLINE + " INTEGER)"
        }
    }

    abstract class TblRecipeIngr{
        companion object{
            val TABLE_NAME = "RecipeIngredient"
            val COL_ID_RECIPE = "id_recipe"
            val COL_ID_INGR = "id_ingredient"
            val COL_INGR_NAME = "name"

            val createTableStr = "CREATE TABLE " + TABLE_NAME + "(" +
                    COL_ID_RECIPE + " INTEGER," +
                    COL_ID_INGR + " INTEGER," +
                    COL_INGR_NAME + " TEXT," +
                    "PRIMARY KEY ($COL_ID_RECIPE,$COL_ID_INGR)," +
                    "FOREIGN KEY($COL_ID_RECIPE) REFERENCES ${TblRecipe.TABLE_NAME}(${TblRecipe.COL_ID}))"
        }

    }

    abstract class TblSavedRecipe{
        companion object{
            val TABLE_NAME = "SavedRecipe"
            val COL_ID_RECIPE = "id_recipe"
            val COL_ID_USER = "id_user"

            val createTableStr = "CREATE TABLE " + TABLE_NAME + "(" +
                    COL_ID_RECIPE + " INTEGER," +
                    COL_ID_USER + " INTEGER," +
                    "PRIMARY KEY ($COL_ID_RECIPE,$COL_ID_USER)," +
                    "FOREIGN KEY($COL_ID_RECIPE) REFERENCES ${TblRecipe.TABLE_NAME}(${TblRecipe.COL_ID})," +
                    "FOREIGN KEY($COL_ID_USER) REFERENCES ${TblUser.TABLE_NAME}(${TblUser.COL_ID}))"
        }
    }


}