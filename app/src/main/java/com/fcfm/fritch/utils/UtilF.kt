package com.fcfm.fritch.utils

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import java.util.*

object UtilF {
    fun base64ToBitmap(img: String): Bitmap {
        val imgByte = Base64.getDecoder().decode(img)
        return BitmapFactory.decodeByteArray(imgByte, 0, imgByte.size)
    }
}