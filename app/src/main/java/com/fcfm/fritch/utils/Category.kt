package com.fcfm.fritch.utils

class Category{
    companion object{
        val ALL = -1
        val SEASONING = 0
        val CUPBOARD = 1
        val FRIDGE = 2
    }

    class Fridge{
        companion object {
            val PROTEINS = 0
            val VEGIES = 1
            val FRUITS = 2
            val SAUCES = 3
            val DISHES = 4
            val LIQUIDS = 5
        }
    }
}