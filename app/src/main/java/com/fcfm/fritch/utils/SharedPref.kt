package com.fcfm.fritch.utils

import android.content.Context

class SharedPref(val context: Context) {
    private val SHARED_NAME = "userpreferences"

    private val managerPrefs = context.getSharedPreferences(SHARED_NAME, Context.MODE_PRIVATE)

    val ACCOUNT = "id_account"
    val USER = "id_user"
    val USERNAME = "username"
    val EMAIL = "email"

    fun <T> save(key:String, value:T){
        val editor = managerPrefs.edit()

        if (value is Int){
            editor.putInt(key, value)
        }
        else if(value is String){
            editor.putString(key, value)
        }

        editor.commit()
    }

    fun getInt(key: String): Int{
        var value = 0
        value = managerPrefs.getInt(key, 0)
        return value
    }

    fun getString(key: String): String{
        var value = ""
        value = managerPrefs.getString(key, "").toString()
        return value
    }

    fun logOut(){
        save(this.ACCOUNT, 0)
        save(this.USER, 0)
        save(this.EMAIL, "")
        save(this.USERNAME, "")
    }
}