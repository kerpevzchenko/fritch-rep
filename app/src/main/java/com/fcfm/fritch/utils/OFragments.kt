package com.fcfm.fritch.utils

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.fcfm.fritch.R
import com.fcfm.fritch.interfaces.IFragments

object OFragments {
    fun loadFragment(fragment: Fragment, supportFragmentManager: FragmentManager, container: Int) {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.add(container, fragment)
        fragmentTransaction.commit()
    }

    fun replaceFragment(fragment: Fragment, supportFragmentManager: FragmentManager, container: Int){
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(container, fragment)
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }

    fun prevFragment(supportFragmentManager: FragmentManager){
        supportFragmentManager.popBackStackImmediate()
    }

    fun clearStack(supportFragmentManager: FragmentManager){
        val count = supportFragmentManager.backStackEntryCount

        for (i in 0..count){
            supportFragmentManager.popBackStack()
        }
    }
}