package com.fcfm.fritch.dialog

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import com.fcfm.fritch.R
import com.fcfm.fritch.interfaces.IDialog
import com.fcfm.fritch.models.User
import kotlinx.android.synthetic.main.dialog_create_user.*
import java.lang.IllegalStateException

class CreateUserDialog(val IDialog: IDialog.User, val id_account: Int) : DialogFragment() {
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        var builder: AlertDialog.Builder? = null

        activity?.let{
            builder = AlertDialog.Builder(it)

            val inflater = requireActivity().layoutInflater

            builder!!.setView(inflater.inflate(R.layout.dialog_create_user, null))
                .setTitle("Titulo")

            //builder!!.setCancelable(false)

        } ?: throw IllegalStateException("Activity cannot be null")

        val dialog = builder!!.create()

        dialog.setOnShowListener {
            val btnCreate = dialog.findViewById<Button>(R.id.dcuCreate_btn)
            btnCreate.setOnClickListener {

                val name = dialog.findViewById<EditText>(R.id.dcuName_txt).text.toString()

                if(name.isNotEmpty()) {
                    showProgressBar()
                    val user = User(null, name, id_account)


                    user.addAPI(
                        {
                            hideProgressBar()

                            user.id_user = it!!
                            IDialog.getUser(user)

                            dialog.dismiss()
                        },
                        {
                            hideProgressBar()
                            IDialog.getUser(user)
                        })
                }
                else{
                    Toast.makeText(activity, R.string.empty_field, Toast.LENGTH_SHORT).show()
                }

            }

            val btnCancel = dialog.findViewById<Button>(R.id.dcuCancel_btn)
            btnCancel.setOnClickListener {
                dialog.dismiss()
            }
        }

        dialog.setCanceledOnTouchOutside(false)
        return dialog
    }

    private fun showProgressBar(){
        dialog!!.dcuName_txt.visibility = View.INVISIBLE
        dialog!!.dcuCreate_btn.visibility = View.INVISIBLE
        dialog!!.dcuCancel_btn.visibility = View.INVISIBLE

        dialog!!.dcu_pb.visibility = View.VISIBLE
    }

    private fun hideProgressBar(){
        dialog!!.dcuName_txt.visibility = View.VISIBLE
        dialog!!.dcuCreate_btn.visibility = View.VISIBLE
        dialog!!.dcuCancel_btn.visibility = View.VISIBLE

        dialog!!.dcu_pb.visibility = View.INVISIBLE
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }
}