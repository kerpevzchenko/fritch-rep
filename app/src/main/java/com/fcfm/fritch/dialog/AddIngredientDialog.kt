package com.fcfm.fritch.dialog

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import com.fcfm.fritch.R
import com.fcfm.fritch.interfaces.IDialog
import com.fcfm.fritch.models.Ingredient
import kotlinx.android.synthetic.main.dialog_add_ingredient.*
import java.lang.ClassCastException
import java.lang.IllegalStateException

class AddIngredientDialog(val iDialog: IDialog.Ingredient, var ingredient: Ingredient) : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let{
            val builder = AlertDialog.Builder(it)

            val inflater = requireActivity().layoutInflater

            builder.setView(inflater.inflate(R.layout.dialog_add_ingredient, null))
                .setPositiveButton(R.string.add, DialogInterface.OnClickListener{
                        dialog, id ->
                    val amountText = getDialog()!!.findViewById<EditText>(R.id.daiAmount_num)
                    val unit = getDialog()!!.daiUnit_spin.selectedItemId



                    if (amountText.text.isEmpty()){
                        Toast.makeText(activity, R.string.empty_field, Toast.LENGTH_SHORT).show()
                    }
                    else{
                        ingredient.amount = amountText.text.toString().toInt()
                        ingredient.unit = unit.toInt()
                        iDialog.getIngredient(ingredient)
                    }

                })
                .setNegativeButton(R.string.cancel, DialogInterface.OnClickListener{
                        dialog, id ->
                    getDialog()!!.cancel()
                })

            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }
}