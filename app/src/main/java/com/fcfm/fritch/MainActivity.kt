package com.fcfm.fritch

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.fcfm.fritch.UserApplication.Companion.dbHelper
import com.fcfm.fritch.UserApplication.Companion.pref
import com.fcfm.fritch.fragments.Seasonings
import com.fcfm.fritch.fragments.login.Choose_user
import com.fcfm.fritch.fragments.login.Login
import com.fcfm.fritch.fragments.login.Signin
import com.fcfm.fritch.interfaces.IFragments
import com.fcfm.fritch.utils.OFragments

class MainActivity : AppCompatActivity(), IFragments {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //deleteDatabase(dbHelper.databaseName)

        val account = pref.getInt(pref.ACCOUNT)
        alreadyLoged(account)


    }

    override fun loadFragment(fragment: Fragment) {
        OFragments.loadFragment(fragment, this.supportFragmentManager, R.id.fragmentContainer)
    }

    override fun replaceFragment(fragment: Fragment){
        OFragments.replaceFragment(fragment, this.supportFragmentManager, R.id.fragmentContainer)
    }

    override fun prevFragment() {
        OFragments.prevFragment(this.supportFragmentManager)
    }

    override fun clearStack() {
        OFragments.clearStack(this.supportFragmentManager)
    }

    fun tostError(throwable: Throwable? = null){
        throwable?.let {
            Toast.makeText(this, it.toString(), Toast.LENGTH_SHORT).show()
        }   ?: run{
            Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show()
        }

    }

    private fun alreadyLoged(account: Int){
        if(account == 0){
            loadFragment(Login())
        }
        else{

            val user = pref.getInt(pref.USER)
            if(user == 0){
                val frag = Choose_user.newInstance(Choose_user.LOGIN)
                replaceFragment(frag)
            } else {
                val intent = Intent(this, MainActivity2::class.java)
                startActivity(intent)
            }


        }
    }
}