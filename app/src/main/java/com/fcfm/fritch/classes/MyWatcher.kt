package com.fcfm.fritch.classes

import android.text.Editable
import android.text.TextWatcher

class MyWatcher(val initText : String, textEqual: (Boolean) -> Unit) : TextWatcher{
    val mInitText = initText
    var mTextEqual: ((Boolean) -> Unit) = textEqual

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        if(initText == s.toString()){
            mTextEqual(true)
        }
        else{
            mTextEqual(false)
        }
    }

    override fun afterTextChanged(s: Editable?) {

    }

    fun setTextEqualFunction(function: (Boolean) -> Unit){
        mTextEqual = function
    }
}