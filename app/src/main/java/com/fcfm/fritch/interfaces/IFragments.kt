package com.fcfm.fritch.interfaces

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager

interface IFragments {
    fun loadFragment(fragment: Fragment)
    fun replaceFragment(fragment: Fragment)
    fun prevFragment()
    fun clearStack()
}