package com.fcfm.fritch.interfaces

import com.fcfm.fritch.models.Ingredient
import com.fcfm.fritch.models.Recipe
import com.fcfm.fritch.models.User

class IRVOnClick{
    interface Ingredient {
        fun onClick(ingredient: com.fcfm.fritch.models.Ingredient)
    }

    interface Recipe{
        fun onClick(recipe: com.fcfm.fritch.models.Recipe)
    }

    interface User{
        fun onClick(user: com.fcfm.fritch.models.User)
    }
}



