package com.fcfm.fritch.interfaces

import com.fcfm.fritch.models.Account
import com.fcfm.fritch.models.Ingredient
import com.fcfm.fritch.models.Recipe
import com.fcfm.fritch.models.User
import retrofit2.Call
import retrofit2.http.*
import java.util.*
import kotlin.collections.HashMap

interface IService {
    /*------------INGREDIENTS------------*/
    @GET("Ingredient/Ingredients")
    fun getIngredients(): Call<List<Ingredient>>

    @POST("Ingredient/Search")
    fun searchIngredient(@Body search: Ingredient.Search):Call<List<Ingredient>>

    @POST("Ingredient/Save")
    fun saveIngredient(@Body ingredient: Ingredient): Call<Int>

    @POST("Ingredient/Update")
    fun updateIngredient(@Body ingredient: Ingredient): Call<Int>

    @GET("Ingredient/Mine/{id}")
    fun getMyIngredients(@Path("id") id:Int): Call<List<Ingredient>>


    @POST("Ingredient/RemoveIngredient")
    fun removeIngredient(@Body ingredient:Ingredient): Call<Int>


    /*------------ACCOUNT------------*/
    @Headers("Content-Type: application/json")
    @POST("Account/Login")
    fun login(@Body account: Account):Call<Int>

    @GET("Account/Users/{id}")
    fun getUsers(@Path("id") id: Int): Call<List<User>>

    @POST("Account/SignUp")
    fun signup(@Body account: Account): Call<Int>

    @POST("Account/AddUser")
    fun addUser(@Body user: User): Call<Int>

    @POST("Account/UpdateUser")
    fun updateUser(@Body user: User): Call<Int>

    @POST("Account/UpdateAccount")
    fun updateAccount(@Body account: Account): Call<Int>


    /*------------RECIPES------------*/
    @Headers("Content-Type: application/json")
    @POST("Recipe/AddRecipe")
    fun addRecipe(@Body recipe: Recipe): Call<Recipe>

    @Headers("Content-Type: application/json")
    @POST("Recipe/AddIngredient")
    fun addIngredientToRecipe(@Body ids: HashMap<String, Int>): Call<Int>

    @GET("Recipe/Ingredients/{id}")
    fun getIngredientsOfRecipe(@Path("id") id:Int): Call<List<Ingredient>>

    @GET("Recipe/Recipes/0")
    fun getRecipes(): Call<List<Recipe>>

    @GET("Recipe/Recipes/{id}")
    fun getRecipe(@Path("id") id:Int): Call<Recipe>

    @GET("Recipe/ByCode/{code}")
    fun getRecipe(@Path("code") code:String): Call<Recipe>

    @POST("Recipe/Save")
    fun downloadRecipe(@Body save: Recipe.SaveData): Call<Int>

    @GET("Recipe/Downloads/{id}")
    fun getDownloads(@Path("id") id:Int): Call<Int>

    @POST("Recipe/Update")
    fun updateRecipe(@Body recipe: Recipe): Call<String>

    @GET("Recipe/Mine/{id}")
    fun getMyRecipes(@Path("id") id:Int): Call<List<Recipe>>

    @GET("Recipe/ByIngredient/{id}")
    fun getByIngredient(@Path("id") id: Int): Call<List<Recipe>>

    @POST("Recipe/Delete")
    fun deleteRecipe(@Body recipe: Recipe) : Call<Int>

    @POST("Recipe/DeleteSaved")
    fun deleteSavedRecipe(@Body recipe: Recipe): Call<Int>
}