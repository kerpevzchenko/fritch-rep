package com.fcfm.fritch.interfaces

import androidx.fragment.app.DialogFragment
import com.fcfm.fritch.models.Ingredient
import com.fcfm.fritch.models.User

class IDialog {
    interface Ingredient{
        fun getIngredient(ingredient: com.fcfm.fritch.models.Ingredient)
    }

    interface User{
        fun getUser(user: com.fcfm.fritch.models.User)
    }

    interface Text{
        fun getText(text: String)
    }
}