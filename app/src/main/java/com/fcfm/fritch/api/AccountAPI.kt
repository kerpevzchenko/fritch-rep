package com.fcfm.fritch.api

import com.fcfm.fritch.interfaces.IService
import com.fcfm.fritch.models.Account
import com.fcfm.fritch.models.User
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AccountAPI {
    companion object{
        fun login(account: Account,onSuccess: (Int?)->Unit, onFail: (t: Throwable) -> Unit){
            val service: IService = RestEngine.getRestEngine().create(IService::class.java)
            val result: Call<Int> = service.login(account)

            result.enqueue(object : Callback<Int>{
                override fun onResponse(call: Call<Int>, response: Response<Int>) {
                    onSuccess(response.body())
                }

                override fun onFailure(call: Call<Int>, t: Throwable) {
                    onFail(t)
                }

            })
        }

        fun getUsers(id: Int, onSuccess: (List<User>?) -> Unit, onFail: (t: Throwable) -> Unit){
            val service: IService = RestEngine.getRestEngine().create(IService::class.java)
            val result: Call<List<User>> = service.getUsers(id)

            result.enqueue(object : Callback<List<User>>{
                override fun onResponse(call: Call<List<User>>, response: Response<List<User>>) {
                    onSuccess(response.body())
                }

                override fun onFailure(call: Call<List<User>>, t: Throwable) {
                    onFail(t)
                }

            })

        }

        fun signUp(account: Account, onSuccess: (Int?) -> Unit, onFail: (t: Throwable) -> Unit){
            val service: IService = RestEngine.getRestEngine().create(IService::class.java)
            val result: Call<Int> = service.signup(account)

            result.enqueue(object : Callback<Int>{
                override fun onResponse(call: Call<Int>, response: Response<Int>) {
                    onSuccess(response.body())
                }

                override fun onFailure(call: Call<Int>, t: Throwable) {
                    onFail(t)
                }

            })
        }



        fun updateAccount(account: Account, onSuccess: (Int?) -> Unit, onFail: (t: Throwable) -> Unit){
            val service: IService = RestEngine.getRestEngine().create(IService::class.java)
            val result: Call<Int> = service.updateAccount(account)

            result.enqueue(object : Callback<Int>{
                override fun onResponse(call: Call<Int>, response: Response<Int>) {
                    onSuccess(response.body())
                }

                override fun onFailure(call: Call<Int>, t: Throwable) {
                    onFail(t)
                }

            })
        }
    }
}