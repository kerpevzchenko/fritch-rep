package com.fcfm.fritch.api

import com.fcfm.fritch.interfaces.IService
import com.fcfm.fritch.models.Ingredient
import com.fcfm.fritch.models.Recipe
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class IngredientAPI {
    companion object{
        fun search(
            search : Ingredient.Search,
            onSuccess : (items: List<Ingredient>?) -> Unit,
            onFail : (t: Throwable) -> Unit
        ){
            val service: IService = RestEngine.getRestEngine().create(IService::class.java)
            val result: Call<List<Ingredient>> = service.searchIngredient(search)

            result.enqueue(object : Callback<List<Ingredient>> {
                override fun onFailure(call: Call<List<Ingredient>>, t: Throwable) {
                    onFail(t)
                }

                override fun onResponse(
                    call: Call<List<Ingredient>>,
                    response: Response<List<Ingredient>>
                ) {
                    onSuccess(response.body())
                }

            })
        }

        fun save(ingredient: Ingredient, onSuccess: (items: Int?) -> Unit, onFail: (t: Throwable) -> Unit){
            val service: IService= RestEngine.getRestEngine().create(IService::class.java)
            val result: Call<Int> = service.saveIngredient(ingredient)

            result.enqueue(object : Callback<Int>{
                override fun onResponse(call: Call<Int>, response: Response<Int>) {
                    onSuccess(response.body())
                }

                override fun onFailure(call: Call<Int>, t: Throwable) {
                    onFail(t)
                }

            })
        }

        fun update(ingredient: Ingredient, onSuccess: (items: Int?) -> Unit, onFail: (t: Throwable) -> Unit){
            val service: IService = RestEngine.getRestEngine().create(IService::class.java)
            val result: Call<Int> = service.updateIngredient(ingredient)

            result.enqueue(object : Callback<Int>{
                override fun onResponse(call: Call<Int>, response: Response<Int>) {
                    onSuccess(response.body())
                }

                override fun onFailure(call: Call<Int>, t: Throwable) {
                    onFail(t)
                }

            })
        }

        fun getMine(id_user: Int, onSuccess: (items: List<Ingredient>?) -> Unit, onFail: (t: Throwable) -> Unit){
            val service: IService = RestEngine.getRestEngine().create(IService::class.java)
            val result: Call<List<Ingredient>> = service.getMyIngredients(id_user)

            result.enqueue(object : Callback<List<Ingredient>>{
                override fun onResponse(
                    call: Call<List<Ingredient>>,
                    response: Response<List<Ingredient>>
                ) {
                    onSuccess(response.body())
                }

                override fun onFailure(call: Call<List<Ingredient>>, t: Throwable) {
                    onFail(t)
                }

            })
        }

        fun remove(ingredient: Ingredient, onSuccess: (res: Int?) -> Unit, onFail: (t: Throwable) -> Unit){
            val service: IService = RestEngine.getRestEngine().create(IService::class.java)
            val result: Call<Int> = service.removeIngredient(ingredient)

            result.enqueue(object : Callback<Int>{
                override fun onResponse(call: Call<Int>, response: Response<Int>) {
                    onSuccess(response.body())
                }

                override fun onFailure(call: Call<Int>, t: Throwable) {
                    onFail(t)
                }

            })
        }
    }
}
