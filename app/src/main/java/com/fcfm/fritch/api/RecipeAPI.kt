package com.fcfm.fritch.api

import com.fcfm.fritch.interfaces.IService
import com.fcfm.fritch.models.Ingredient
import com.fcfm.fritch.models.Recipe
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RecipeAPI {
    companion object{
        fun addRecipe(recipe: Recipe, onSuccess: (recipe : Recipe?) -> Unit, onFail: (t : Throwable) -> Unit){
            val service: IService = RestEngine.getRestEngine().create(IService::class.java)
            val result: Call<Recipe> = service.addRecipe(recipe)

            result.enqueue(object : Callback<Recipe> {
                override fun onResponse(
                    call: Call<Recipe>,
                    response: Response<Recipe>
                ) {
                    onSuccess(response.body())
                }

                override fun onFailure(call: Call<Recipe>, t: Throwable) {
                    onFail(t)
                }

            })
        }

        fun addIngredient(id_ingredient: Int, id_recipe: Int,
                          onSuccess: (Int?) -> Unit,
                          onFail: (t: Throwable) -> Unit){
            val hashMap = HashMap<String, Int>()
            hashMap["id_ingredient"] = id_ingredient
            hashMap["id_recipe"] = id_recipe
            val service: IService = RestEngine.getRestEngine().create(IService::class.java)
            val result: Call<Int> = service.addIngredientToRecipe(hashMap)

            result.enqueue(object : Callback<Int>{
                override fun onResponse(call: Call<Int>, response: Response<Int>) {
                    onSuccess(response.body())
                }

                override fun onFailure(call: Call<Int>, t: Throwable) {
                    onFail(t)
                }

            })
        }

        fun getIngredients(id: Int,
                           onSuccess: (List<Ingredient>?) -> Unit,
                           onFail: (t: Throwable) -> Unit){
            val service: IService = RestEngine.getRestEngine().create(IService::class.java)
            val result: Call<List<Ingredient>> = service.getIngredientsOfRecipe(id)

            result.enqueue(object : Callback<List<Ingredient>>{
                override fun onResponse(
                    call: Call<List<Ingredient>>,
                    response: Response<List<Ingredient>>
                ) {
                    onSuccess(response.body())
                }

                override fun onFailure(call: Call<List<Ingredient>>, t: Throwable) {
                    onFail(t)
                }

            })
        }

            fun getRecipe(id: Int,
                          onSuccess: (Recipe?) -> Unit,
                      onFail: (t: Throwable) -> Unit){
            val service: IService = RestEngine.getRestEngine().create(IService::class.java)
            val result: Call<Recipe> = service.getRecipe(id)

            result.enqueue(object : Callback<Recipe>{
                override fun onResponse(
                    call: Call<Recipe>,
                    response: Response<Recipe>
                ) {
                    onSuccess(response.body())
                }

                override fun onFailure(call: Call<Recipe>, t: Throwable) {
                    onFail(t)
                }

            })
        }

        fun getRecipe(code: String,
                      onSuccess: (Recipe?) -> Unit,
                      onFail: (t: Throwable) -> Unit){
            val service: IService = RestEngine.getRestEngine().create(IService::class.java)
            val result: Call<Recipe> = service.getRecipe(code)

            result.enqueue(object : Callback<Recipe>{
                override fun onResponse(
                    call: Call<Recipe>,
                    response: Response<Recipe>
                ) {
                    onSuccess(response.body())
                }

                override fun onFailure(call: Call<Recipe>, t: Throwable) {
                    onFail(t)
                }

            })
        }

        fun getRecipes(onSuccess: (List<Recipe>?) -> Unit,
                       onFail: (t: Throwable) -> Unit){
            val service: IService = RestEngine.getRestEngine().create(IService::class.java)
            val result: Call<List<Recipe>> = service.getRecipes()

            result.enqueue(object : Callback<List<Recipe>>{
                override fun onResponse(
                    call: Call<List<Recipe>>,
                    response: Response<List<Recipe>>
                ) {
                    onSuccess(response.body())
                }

                override fun onFailure(call: Call<List<Recipe>>, t: Throwable) {
                    onFail(t)
                }

            })
        }

        fun download(saveData: Recipe.SaveData, onSuccess: (items: Int?) -> Unit, onFail: (t: Throwable) -> Unit){
            val service: IService = RestEngine.getRestEngine().create(IService::class.java)
            val result: Call<Int> = service.downloadRecipe(saveData)

            result.enqueue(object : Callback<Int>{
                override fun onResponse(call: Call<Int>, response: Response<Int>) {
                    onSuccess(response.body())
                }

                override fun onFailure(call: Call<Int>, t: Throwable) {
                    onFail(t)
                }

            })
        }

        fun getDownloads(id:Int, onSuccess: (Int?) -> Unit, onFail: (t: Throwable) -> Unit){
            val service: IService = RestEngine.getRestEngine().create(IService::class.java)
            val result:Call<Int> = service.getDownloads(id)

            result.enqueue(object : Callback<Int>{
                override fun onResponse(call: Call<Int>, response: Response<Int>) {
                    onSuccess(response.body())
                }

                override fun onFailure(call: Call<Int>, t: Throwable) {
                    onFail(t)
                }

            })
        }

        fun update(recipe: Recipe, onSuccess: (String?) -> Unit, onFail: (t: Throwable) -> Unit){
            val service: IService = RestEngine.getRestEngine().create(IService::class.java)
            val result:Call<String> = service.updateRecipe(recipe)

            result.enqueue(object : Callback<String>{
                override fun onResponse(call: Call<String>, response: Response<String>) {
                    onSuccess(response.body())
                }

                override fun onFailure(call: Call<String>, t: Throwable) {
                    onFail(t)
                }

            })
        }

        fun searchByCode(code:String, onSuccess: (Recipe?) -> Unit, onFail: (t: Throwable) -> Unit){
            val service: IService = RestEngine.getRestEngine().create(IService::class.java)
            val result:Call<Recipe> = service.getRecipe(code)

            result.enqueue(object : Callback<Recipe>{
                override fun onResponse(call: Call<Recipe>, response: Response<Recipe>) {
                    onSuccess(response.body())
                }

                override fun onFailure(call: Call<Recipe>, t: Throwable) {
                    onFail(t)
                }

            })
        }

        fun getMine(id_user: Int, onSuccess: (items: List<Recipe>?) -> Unit, onFail: (t: Throwable) -> Unit){
            val service: IService = RestEngine.getRestEngine().create(IService::class.java)
            val result: Call<List<Recipe>> = service.getMyRecipes(id_user)

            result.enqueue(object : Callback<List<Recipe>>{
                override fun onResponse(
                    call: Call<List<Recipe>>,
                    response: Response<List<Recipe>>
                ) {
                    onSuccess(response.body())
                }

                override fun onFailure(call: Call<List<Recipe>>, t: Throwable) {
                    onFail(t)
                }

            })
        }

        fun getByIngredient(id_ingredient: Int, onSuccess: (items: List<Recipe>?) -> Unit, onFail: (t: Throwable) -> Unit){
            val service: IService = RestEngine.getRestEngine().create(IService::class.java)
            val result: Call<List<Recipe>> = service.getByIngredient(id_ingredient)

            result.enqueue(object : Callback<List<Recipe>>{
                override fun onResponse(
                    call: Call<List<Recipe>>,
                    response: Response<List<Recipe>>
                ) {
                    onSuccess(response.body())
                }

                override fun onFailure(call: Call<List<Recipe>>, t: Throwable) {
                    onFail(t)
                }

            })
        }

        fun delete(recipe: Recipe, onSuccess: (Int?) -> Unit, onFail: (t: Throwable) -> Unit){
            val service: IService = RestEngine.getRestEngine().create(IService::class.java)
            val result: Call<Int> = service.deleteRecipe(recipe)

            result.enqueue(object : Callback<Int>{
                override fun onResponse(call: Call<Int>, response: Response<Int>) {
                    onSuccess(response.body())
                }

                override fun onFailure(call: Call<Int>, t: Throwable) {
                    onFail(t)
                }

            })
        }

        fun deleteSaved(recipe: Recipe, onSuccess: (Int?) -> Unit, onFail: (t: Throwable) -> Unit){
            val service: IService = RestEngine.getRestEngine().create(IService::class.java)
            val result: Call<Int> = service.deleteSavedRecipe(recipe)

            result.enqueue(object : Callback<Int>{
                override fun onResponse(call: Call<Int>, response: Response<Int>) {
                    onSuccess(response.body())
                }

                override fun onFailure(call: Call<Int>, t: Throwable) {
                    onFail(t)
                }

            })
        }
    }
}