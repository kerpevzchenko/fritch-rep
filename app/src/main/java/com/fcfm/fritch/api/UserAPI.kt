package com.fcfm.fritch.api

import com.fcfm.fritch.interfaces.IService
import com.fcfm.fritch.models.Account
import com.fcfm.fritch.models.User
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UserAPI {
    companion object{
        fun addUser(user: User, onSuccess: (Int?) -> Unit, onFail: (t: Throwable) -> Unit){
            val service: IService = RestEngine.getRestEngine().create(IService::class.java)
            val result: Call<Int> = service.addUser(user)

            result.enqueue(object : Callback<Int> {
                override fun onResponse(call: Call<Int>, response: Response<Int>) {
                    onSuccess(response.body())
                }

                override fun onFailure(call: Call<Int>, t: Throwable) {
                    onFail(t)
                }

            })
        }

        fun update(user: User, onSuccess: (Int?) -> Unit, onFail: (t: Throwable) -> Unit){
            val service: IService = RestEngine.getRestEngine().create(IService::class.java)
            val result: Call<Int> = service.updateUser(user)

            result.enqueue(object : Callback<Int>{
                override fun onResponse(call: Call<Int>, response: Response<Int>) {
                    onSuccess(response.body())
                }

                override fun onFailure(call: Call<Int>, t: Throwable) {
                    onFail(t)
                }

            })

        }
    }
}