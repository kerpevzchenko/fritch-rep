package com.fcfm.fritch

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.view.MenuItem
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.fcfm.fritch.UserApplication.Companion.dbHelper
import com.fcfm.fritch.UserApplication.Companion.pref
import com.fcfm.fritch.fragments.Cupboard
import com.fcfm.fritch.fragments.fridge.Fridge
import com.fcfm.fritch.fragments.recipes.Recipes
import com.fcfm.fritch.fragments.Seasonings
import com.fcfm.fritch.fragments.recipes.AddRecipe
import com.fcfm.fritch.fragments.recipes.EditRecipe
import com.fcfm.fritch.interfaces.IFragments
import com.fcfm.fritch.models.Ingredient
import com.fcfm.fritch.models.Recipe
import com.fcfm.fritch.utils.OFragments
import com.google.android.material.navigation.NavigationBarView
import kotlinx.android.synthetic.main.activity_main2.*
import java.io.ByteArrayOutputStream

class MainActivity2 : AppCompatActivity(), IFragments, NavigationBarView.OnItemSelectedListener {
    companion object{
        private val IMAGE_PICK_CODE = 1000;
        private val PERMISSION_CODE = 1001;
        private val CAMERA_CODE = 1002;
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        //deleteDatabase(dbHelper.databaseName)


        Ingredient.getMyIngredients(pref.getInt(pref.ACCOUNT),
            {
                if (it != null) {
                    for (item in it){
                        item.insert()
                        item.online = true
                        item.updateOnline()
                    }

                    Recipe.getMyRecipe(pref.getInt(pref.USER),
                        {
                            if(it != null){
                                for (item in it){
                                    item.getIngredientsAPI(
                                        {
                                            item.ingredients = it as MutableList<Ingredient>?
                                            item.insert()
                                        },
                                        {
                                            tostError(it)
                                        })
                                }
                            }
                            replaceFragment(Recipes())
                        },
                        {
                            tostError(it)
                        })
                }
            },
            {
                //tostError(it)
                replaceFragment(Recipes())
            })

        //replaceFragment(Recipes())

        bottomNavigationView.setOnItemSelectedListener(this)
    }

    override fun onStart() {
        super.onStart()

        val user = pref.getInt(pref.USER)
        val username = pref.getString(pref.USERNAME)
    }

    override fun loadFragment(fragment: Fragment) {
        OFragments.loadFragment(fragment, this.supportFragmentManager, R.id.fragmentContainer2)
    }

    override fun replaceFragment(fragment: Fragment) {
        OFragments.replaceFragment(fragment, this.supportFragmentManager, R.id.fragmentContainer2)
    }

    override fun prevFragment() {
        OFragments.prevFragment(this.supportFragmentManager)
    }

    override fun clearStack() {
        OFragments.clearStack(this.supportFragmentManager)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.bnm_r -> {
                OFragments.replaceFragment(
                    Recipes(),
                    this.supportFragmentManager,
                    fragmentContainer2.id
                )
                return true
            }

            R.id.bnm_s -> {
                OFragments.replaceFragment(
                    Seasonings(),
                    this.supportFragmentManager,
                    fragmentContainer2.id
                )
                return true
            }

            R.id.bnm_c -> {
                OFragments.replaceFragment(
                    Cupboard(),
                    this.supportFragmentManager,
                    fragmentContainer2.id
                )
                return true
            }

            R.id.bnm_f -> {
                OFragments.replaceFragment(
                    Fridge(),
                    this.supportFragmentManager,
                    fragmentContainer2.id
                )
                return true
            }

            R.id.bnm_a -> {
                OFragments.replaceFragment(
                    com.fcfm.fritch.fragments.account.Account(),
                    this.supportFragmentManager,
                    fragmentContainer2.id
                )

                return true
            }

            else -> return false
        }
    }

    private fun getCurrentFragment(): Fragment? {
        return supportFragmentManager.findFragmentById(R.id.fragmentContainer2)
    }

    fun pickImageFromGallery(){
        val intent = Intent()
        intent.setAction(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, IMAGE_PICK_CODE)
    }

    fun tostError(throwable: Throwable? = null){
        throwable?.let {
            Toast.makeText(this, it.toString(), Toast.LENGTH_SHORT).show()
        }   ?: run{
            Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show()
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(resultCode == Activity.RESULT_OK){
            when(requestCode){
                IMAGE_PICK_CODE -> {
                    val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, data!!.data)
                    val baos = ByteArrayOutputStream()
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 30, baos)

                    val frag = getCurrentFragment()
                    if(frag is EditRecipe){
                        frag.getImage(
                            baos.toByteArray()
                            //bitmap
                        )
                    }
                }
            }
        }
    }

    /*private fun getBytes(uri:Uri): ByteArray? {
        val iStream = this.contentResolver.openInputStream(uri)

        try{
            return getBytes(iStream!!)
        } finally {
            try{
                iStream!!.close()
            } catch (ignored : IOException) {  }
        }
    }

    private fun getBytes(iStream: InputStream): ByteArray? {
        var bytesResult: ByteArray? = null
        val byteBuffer = ByteArrayOutputStream()
        val bufferSize = 1024
        val buffer = ByteArray(bufferSize)
        try{
            var len = 0
            len = iStream.read(buffer)
            while (len != -1){
                byteBuffer.write(buffer, 0, len)
                len = iStream.read(buffer)
            }
            bytesResult = byteBuffer.toByteArray()
        } finally {
            try {
                byteBuffer.close()
            } catch (ingored: IOException){  }
        }

        return bytesResult
    }*/

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode){
            PERMISSION_CODE -> {
                if(grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    pickImageFromGallery()
                }
                else{
                    Toast.makeText(this, getString(R.string.permission_denied).toString(), Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    fun checkReadExternalStoragePermission(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            var boolDo: Boolean = false
            if(checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED){
                val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)
                requestPermissions(permissions, PERMISSION_CODE)
            }
            else{
                boolDo = true
            }

            if(boolDo == true){
                pickImageFromGallery()
            }
        }
    }
}