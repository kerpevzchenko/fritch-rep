package com.fcfm.fritch

import android.app.Application
import com.fcfm.fritch.interfaces.IFragments
import com.fcfm.fritch.models.DataDbHelper
import com.fcfm.fritch.utils.OFragments
import com.fcfm.fritch.utils.SharedPref

class UserApplication : Application() {
    companion object{
        lateinit var dbHelper: DataDbHelper
        lateinit var pref: SharedPref
    }

    override fun onCreate() {
        super.onCreate()
        dbHelper = DataDbHelper(applicationContext)
        pref = SharedPref(applicationContext)
    }
}