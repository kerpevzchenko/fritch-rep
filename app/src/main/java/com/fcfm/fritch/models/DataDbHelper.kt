package com.fcfm.fritch.models

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import android.widget.Toast
import com.fcfm.fritch.utils.Category
import com.fcfm.fritch.utils.SetDB
import java.lang.Exception

class DataDbHelper(var context: Context) : SQLiteOpenHelper(context, SetDB.DB_NAME, null, SetDB.DB_VERSION) {

    override fun onCreate(db: SQLiteDatabase?) {
        try{
            db?.execSQL(SetDB.TblMyIngredient.createTableStr)

            db?.execSQL(SetDB.TblRecipe.createTableStr)

            db?.execSQL(SetDB.TblRecipeIngr.createTableStr)
            
            db?.execSQL(SetDB.TblSavedRecipe.createTableStr)


            Log.e("Exito", "Creo tablas")
        }
        catch (e: Exception)
        {
            Log.e("Exception", e.toString())
        }
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("Not yet implemented")
    }

    public fun test():String{
        return "test"
    }




}