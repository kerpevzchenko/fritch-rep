package com.fcfm.fritch.models

import com.fcfm.fritch.api.AccountAPI

data class Account(
    var id_account: Int? = null,
    var email: String? = null,
    var pass: String? = null
) {
    fun login(onSuccess: (Int?) -> Unit, onFail: (t:Throwable) -> Unit){
        AccountAPI.login(this, onSuccess, onFail)
    }

    fun signup(onSuccess: (Int?) -> Unit, onFail: (t: Throwable) -> Unit){
        AccountAPI.signUp(this, onSuccess, onFail)
    }

    fun getUsers(onSuccess: (List<User>?) -> Unit, onFail: (t:Throwable) -> Unit){
        AccountAPI.getUsers(this.id_account!!, onSuccess, onFail)
    }

    fun update(onSuccess: (Int?) -> Unit, onFail: (t: Throwable) -> Unit){
        AccountAPI.updateAccount(this, onSuccess, onFail)
    }
}