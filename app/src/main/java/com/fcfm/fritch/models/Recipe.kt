package com.fcfm.fritch.models

import android.content.ContentValues
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import com.fcfm.fritch.UserApplication.Companion.dbHelper
import com.fcfm.fritch.api.RecipeAPI
import com.fcfm.fritch.utils.SetDB
import java.lang.Exception
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.ArrayList

/**
 * @param online Se usa para saber si la receta esta guardada en la base de datos general
 */
data class Recipe(
    var id_recipe: Int? = null,
    var name: String? = null,
    var portions: Int? = null,
    var type: Int? = null,
    var ingredients: MutableList<Ingredient>? = null,
    var descr: String? = null,
    var code: String? = null,
    var user: User? = null,
    var img: String? = null,
    @Transient var online: Boolean = false,
    var downloads: Int? = null,
    var date: String? = null,
    var last_update: String? = null
){
    class SaveData(
        var id_recipe: Int? = null,
        var id_user: Int? = null
    )

    companion object{
        fun getRecipe(id: Int,
                         onSuccess: (Recipe?) -> Unit,
                         onFail: (t: Throwable) -> Unit){
            RecipeAPI.getRecipe(id, onSuccess, onFail)

        }

        fun getRecipe(code: String,
                         onSuccess: (Recipe?) -> Unit,
                         onFail: (t: Throwable) -> Unit){
            RecipeAPI.getRecipe(code, onSuccess, onFail)
        }

        fun getRecipes(onSuccess: (List<Recipe>?) -> Unit,
                         onFail: (t: Throwable) -> Unit){
            RecipeAPI.getRecipes(onSuccess, onFail)
        }

        fun getMyRecipes(id_user: Int) : MutableList<Recipe>{
            val list:MutableList<Recipe> =  ArrayList()
            val table =  SetDB.TblRecipe
            val database: SQLiteDatabase = dbHelper.writableDatabase
            val columns: Array<String> = arrayOf(
                table.COL_ID,
                table.COL_NAME,
                table.COL_ID_USER,
                table.COL_USER_NAME,
                table.COL_PORTIONS,
                table.COL_TYPE,
                table.COL_IMG
            )

            val where = table.COL_ID_USER + "=" + id_user

            val data = database.query(
                table.TABLE_NAME,
                columns,
                where,
                null,
                null,
                null,
                null
            )

            if(data.moveToFirst()){
                do{

                    val img = data.getBlob(data.getColumnIndex(table.COL_IMG))
                    val user = User(data.getInt(data.getColumnIndex(table.COL_ID_USER)), data.getString(data.getColumnIndex(table.COL_USER_NAME)))
                    val recipe = Recipe(
                        id_recipe = data.getInt(data.getColumnIndex(table.COL_ID)),
                        name = data.getString(data.getColumnIndex(table.COL_NAME)),
                        user = user,
                        portions = data.getInt(data.getColumnIndex(table.COL_PORTIONS)),
                        type = data.getInt(data.getColumnIndex(table.COL_TYPE)),
                        img = Base64.getEncoder().encodeToString(img)
                    )

                    list.add(recipe)
                }while (data.moveToNext())
            }

            database.close()

            return list
        }

        fun getRecipe(id_recipe: Int?): Recipe?{
            var recipe: Recipe? = null
            val table =  SetDB.TblRecipe
            val database: SQLiteDatabase = dbHelper.writableDatabase
            val columns: Array<String> = arrayOf(
                table.COL_ID,
                table.COL_NAME,
                table.COL_DESCR,
                table.COL_USER_NAME,
                table.COL_CODE,
                table.COL_PORTIONS,
                table.COL_TYPE,
                table.COL_IMG,
                table.COL_DATE
            )

            val where = table.COL_ID + "=" + id_recipe

            val data = database.query(
                table.TABLE_NAME,
                columns,
                where,
                null,
                null,
                null,
                null
            )

            if(data.moveToFirst()){
                do{
                    val img = data.getBlob(data.getColumnIndex(table.COL_IMG))
                    val user = User(null, data.getString(data.getColumnIndex(table.COL_USER_NAME)))
                    recipe = Recipe(
                        id_recipe = data.getInt(data.getColumnIndex(table.COL_ID)),
                        name = data.getString(data.getColumnIndex(table.COL_NAME)),
                        user = user,
                        descr = data.getString(data.getColumnIndex(table.COL_DESCR)),
                        code = data.getString(data.getColumnIndex(table.COL_CODE)),
                        portions = data.getInt(data.getColumnIndex(table.COL_PORTIONS)),
                        type = data.getInt(data.getColumnIndex(table.COL_TYPE)),
                        img = Base64.getEncoder().encodeToString(img),
                        date = data.getString(data.getColumnIndex(table.COL_DATE))
                    )
                    val listIngredient = recipe.getIngredientsFromSqLite()
                    recipe.ingredients = listIngredient

                }while (data.moveToNext())
            }

            database.close()

            return recipe
        }

        fun getSavedRecipes(id_user: Int): MutableList<Recipe>{
            val list:MutableList<Recipe> =  ArrayList()
            val tableRecipe = SetDB.TblRecipe
            val tableSaved = SetDB.TblSavedRecipe
            val database : SQLiteDatabase = dbHelper.writableDatabase
            val columns: Array<String> = arrayOf(
                tableRecipe.COL_ID,
                tableRecipe.COL_NAME,
                tableRecipe.COL_USER_NAME,
                tableRecipe.COL_PORTIONS,
                tableRecipe.COL_TYPE,
                tableRecipe.COL_IMG,
                tableRecipe.COL_ID_USER,
                tableRecipe.COL_USER_NAME
            )

            val query = "SELECT " +
                    "${tableRecipe.TABLE_NAME}.${columns[0]}, " +
                    "${tableRecipe.TABLE_NAME}.${columns[1]}, " +
                    "${tableRecipe.TABLE_NAME}.${columns[2]}, " +
                    "${tableRecipe.TABLE_NAME}.${columns[3]}, " +
                    "${tableRecipe.TABLE_NAME}.${columns[4]}, " +
                    "${tableRecipe.TABLE_NAME}.${columns[5]}, " +
                    "${tableRecipe.TABLE_NAME}.${columns[6]}, " +
                    "${tableRecipe.TABLE_NAME}.${columns[7]} " +
                    "FROM ${tableRecipe.TABLE_NAME} " +
                    "INNER JOIN ${tableSaved.TABLE_NAME} ON ${tableSaved.TABLE_NAME}.${tableSaved.COL_ID_RECIPE} = ${tableRecipe.TABLE_NAME}.${tableRecipe.COL_ID} " +
                    "WHERE  ${tableSaved.TABLE_NAME}.${tableSaved.COL_ID_USER} = $id_user "

            val data = database.rawQuery(query,null)

            if(data.moveToFirst()){
                do{

                    val img = data.getBlob(data.getColumnIndex(tableRecipe.COL_IMG))
                    val user = User(data.getInt(data.getColumnIndex(tableRecipe.COL_ID_USER)), data.getString(data.getColumnIndex(tableRecipe.COL_USER_NAME)))
                    val recipe = Recipe(
                        id_recipe = data.getInt(data.getColumnIndex(tableRecipe.COL_ID)),
                        name = data.getString(data.getColumnIndex(tableRecipe.COL_NAME)),
                        user = user,
                        portions = data.getInt(data.getColumnIndex(tableRecipe.COL_PORTIONS)),
                        type = data.getInt(data.getColumnIndex(tableRecipe.COL_TYPE)),
                        img = Base64.getEncoder().encodeToString(img)
                    )

                    list.add(recipe)
                }while (data.moveToNext())
            }

            database.close()

            return list
        }

        fun getMyRecipe(id_user: Int, onSuccess: (items: List<Recipe>?) -> Unit, onFail: (t: Throwable) -> Unit){
            RecipeAPI.getMine(id_user, onSuccess, onFail)
        }

        fun getByIngredient(id_ingredient: Int, onSuccess: (items: List<Recipe>?) -> Unit, onFail: (t: Throwable) -> Unit){
            RecipeAPI.getByIngredient(id_ingredient, onSuccess, onFail)
        }
    }

    /**
     * Agrega la receta a la base de tados general.
     */
    fun addRecipe(onSuccess: (recipe : Recipe?) -> Unit,
                     onFail: (t : Throwable) -> Unit){
        RecipeAPI.addRecipe(this, onSuccess, onFail)
    }

    fun addIngredientAPI(id_ingredient: Int, onSuccess: (Int?) -> Unit, onFail: (t: Throwable) -> Unit){
        RecipeAPI.addIngredient(id_ingredient,this.id_recipe!!, onSuccess, onFail)
    }

    fun getIngredientsAPI(onSuccess: (List<Ingredient>?) -> Unit, onFail: (t: Throwable) -> Unit){
        RecipeAPI.getIngredients(this.id_recipe!!, onSuccess, onFail)
    }


    /**
     * Inserta la receta en la base de datos SQLITE
     */
    fun insert(): Boolean{
        val table = SetDB.TblRecipe
        val database = dbHelper.writableDatabase
        val values: ContentValues = ContentValues()
        var boolResult: Boolean = true

        values.put(table.COL_ID, this.id_recipe)
        values.put(table.COL_NAME, this.name)
        values.put(table.COL_DESCR, this.descr)
        values.put(table.COL_PORTIONS, this.portions)
        values.put(table.COL_TYPE, this.type)
        values.put(table.COL_ID_USER, this.user!!.id_user)
        values.put(table.COL_USER_NAME, this.user!!.name)
        values.put(table.COL_CODE, this.code)
        values.put(table.COL_IMG, Base64.getDecoder().decode(this.img))
        values.put(table.COL_LAST_UPDATE, this.last_update)
        values.put(table.COL_DATE, this.date)

        boolResult = try{

            for(ingredient in this.ingredients!!){
                if (!ingredient.insertIntoRecipeIngredient(this.id_recipe!!)){
                    return false
                }
            }

            val result = database.insert(table.TABLE_NAME, null, values)

            result != (-1).toLong()
        } catch (e: Exception){
            Log.e("Exception", e.toString())
            false
        }

        database.close()

        return boolResult

    }

    /**
     * Inserta la receta en la base de datos SQLITE
     */
    fun download(id_user: Int) : Boolean{
        this.insertInSavedRecipe(id_user)
        return this.insert()
    }

    fun getIngredientsFromSqLite(): MutableList<Ingredient>{
        val list:MutableList<Ingredient> =  ArrayList()
        val tableRecipeInt = SetDB.TblRecipeIngr
        val tableIngredient = SetDB.TblMyIngredient
        val database: SQLiteDatabase = dbHelper.writableDatabase
        val columns: Array<String> = arrayOf(
            tableRecipeInt.COL_ID_INGR
        )

        val where = tableRecipeInt.COL_ID_RECIPE + "=" + this.id_recipe

        val query =
                "SELECT  ${tableRecipeInt.TABLE_NAME}.${tableRecipeInt.COL_ID_INGR}, ${tableRecipeInt.COL_INGR_NAME}" +
                " FROM ${tableRecipeInt.TABLE_NAME}" +
                " WHERE ${tableRecipeInt.COL_ID_RECIPE} = ${this.id_recipe}"

        val data = database.rawQuery(query,null)

        if(data.moveToFirst()){
            do{

                val ingredient = Ingredient(
                    data.getInt(data.getColumnIndex(tableRecipeInt.COL_ID_INGR)),
                    data.getString(data.getColumnIndex(tableIngredient.COL_NAME))
                )

                list.add(ingredient)
            }while (data.moveToNext())
        }

        database.close()

        return list
    }

    /**
     * Guarda la receta descargada en la base de datos general
     */
    fun Download(saveData: SaveData, onSuccess: (Int?) -> Unit, onFail: (t: Throwable) -> Unit){
        RecipeAPI.download(saveData, onSuccess, onFail)
    }

    fun GetDownloads(onSuccess: (Int?) -> Unit, onFail: (t: Throwable) -> Unit){
        RecipeAPI.getDownloads(this.id_recipe!!, onSuccess, onFail)
    }

    fun updateOnline(): Boolean{
        val table = SetDB.TblRecipe
        val database : SQLiteDatabase = dbHelper.writableDatabase
        val values: ContentValues = ContentValues()
        var boolResult: Boolean = true

        values.put(table.COL_ONLINE, this.online)

        val where = table.COL_ID + " = " + this.id_recipe

        boolResult = try{
            val result = database.update(table.TABLE_NAME, values, where, null)

            result > 0
        } catch (e: Exception){
            Log.e("Exception", e.toString())
            false
        }

        database.close()
        return boolResult
    }

    fun insertInSavedRecipe(id_user: Int): Boolean{
        val table = SetDB.TblSavedRecipe
        val database = dbHelper.writableDatabase
        val values: ContentValues = ContentValues()
        var boolResult: Boolean = true

        values.put(table.COL_ID_RECIPE, this.id_recipe)
        values.put(table.COL_ID_USER, id_user)

        boolResult = try{
            val result = database.insert(table.TABLE_NAME, null, values)

            result != (-1).toLong()
        } catch (e: Exception){
            Log.e("Exception", e.toString())
            false
        }

        database.close()

        return boolResult
    }

    fun alreadySaved(id_user: Int): Boolean{
        val table = SetDB.TblSavedRecipe
        val database = dbHelper.writableDatabase
        var count = 0
        val where = table.COL_ID_RECIPE + " = " + this.id_recipe + " AND " + table.COL_ID_USER + " = " + id_user

        val query = "SELECT COUNT() AS recipe " +
                "FROM ${table.TABLE_NAME} " +
                "WHERE $where"

        val data = database.rawQuery(query,null)

        if(data.moveToFirst()){
            do{

                count = data.getInt(data.getColumnIndex("recipe"))

            }while (data.moveToNext())
        }

        database.close()

        return count == 1
    }

    fun updateLastUpdate(): Boolean{
        val table = SetDB.TblRecipe
        val database = dbHelper.writableDatabase
        val values = ContentValues()
        var boolResult: Boolean = true

        values.put(table.COL_LAST_UPDATE, this.last_update)

        val where = table.COL_ID + " = " + this.id_recipe

        boolResult = try{
            val result = database.update(table.TABLE_NAME, values, where, null)

            result != -1
        } catch (e: Exception){
            Log.e("Exception", e.toString())
            false
        }

        database.close()

        return boolResult
    }

    fun updateDate(): Boolean{
        val table = SetDB.TblRecipe
        val database = dbHelper.writableDatabase
        val values = ContentValues()
        var boolResult: Boolean = true

        values.put(table.COL_DATE, this.date)

        val where = table.COL_ID + " = " + this.id_recipe

        boolResult = try{
            val result = database.update(table.TABLE_NAME, values, where, null)

            result != -1
        } catch (e: Exception){
            Log.e("Exception", e.toString())
            false
        }

        database.close()

        return boolResult
    }

    fun update(): Boolean{
        val table = SetDB.TblRecipe
        val database = dbHelper.writableDatabase
        val values = ContentValues()
        var boolResult: Boolean = true

        values.put(table.COL_NAME, this.name)
        values.put(table.COL_DESCR, this.descr)
        values.put(table.COL_PORTIONS, this.portions)
        values.put(table.COL_TYPE, this.type)
        values.put(table.COL_IMG, Base64.getDecoder().decode(this.img))
        values.put(table.COL_LAST_UPDATE, this.last_update)
        values.put(table.COL_ONLINE, this.online)

        val where = table.COL_ID + " = " + this.id_recipe



        boolResult = try{
            val result = database.update(table.TABLE_NAME, values, where, null)

            result != -1
        } catch (e: Exception){
            Log.e("Exception", e.toString())
            false
        }

        database.close()

        return boolResult
    }

    fun update(onSuccess: (String?) -> Unit, onFail: (t: Throwable) -> Unit){
        RecipeAPI.update(this, onSuccess, onFail)
    }

    fun delete(isSavedRecipe: Boolean):Boolean{
        val tableSR = SetDB.TblSavedRecipe
        val tableRI = SetDB.TblRecipeIngr
        val tableR = SetDB.TblRecipe
        val database = dbHelper.writableDatabase
        var boolResult: Boolean = true

        try{

            if(isSavedRecipe)
                database.delete(tableSR.TABLE_NAME, "${tableSR.COL_ID_RECIPE} = ${this.id_recipe}", null)

            val x = database.delete(tableRI.TABLE_NAME, "${tableRI.COL_ID_RECIPE} = ${this.id_recipe}", null)

            val y = database.delete(tableR.TABLE_NAME, "${tableR.COL_ID} = ${this.id_recipe}", null)
        }
        catch (e: Exception){
            Log.e("SQLite delete", e.toString())
            boolResult = false
        }

        database.close()

        return boolResult
    }

    fun delete(onSuccess: (Int?) -> Unit, onFail: (t: Throwable) -> Unit){
        RecipeAPI.delete(this, onSuccess, onFail)
    }

    fun deleteSaved(onSuccess: (Int?) -> Unit, onFail: (t: Throwable) -> Unit){
        RecipeAPI.deleteSaved(this, onSuccess, onFail)
    }

}



