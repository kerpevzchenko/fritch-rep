package com.fcfm.fritch.models


import android.content.ContentValues
import android.database.sqlite.SQLiteConstraintException
import android.database.sqlite.SQLiteDatabase
import android.util.Log
import com.fcfm.fritch.UserApplication.Companion.dbHelper
import com.fcfm.fritch.api.IngredientAPI
import com.fcfm.fritch.utils.Category
import com.fcfm.fritch.utils.SetDB
import java.lang.Exception
import java.lang.NumberFormatException

/**
 * @param type Solo se usa cuando [category] sea [Category.FRIDGE]
 * @param id_account Solo se usa al llamar la API Save. Indica que cuenta guarda el ingrediente.
 * @param online Se usa para saber si el ingrediente esta guardado en la base de datos general
 */
data class Ingredient(
    var id_ingredient: Int? = null,
    var name: String? = null,
    var amount: Int? = null,
    var category: Int? = null,
    var unit: Int? = null,
    var type: Int? = null,
    var id_account: Int? = null,
    @Transient var checked: Boolean = false,
    @Transient var online: Boolean = false
) {
    /**
     * Usado en el Body en la API Search
     */
    class Search(
        var text: String? = null,
        var id_account: Int? = null
    )


    companion object{

        fun getCheckedFromList(list: MutableList<Ingredient>): List<Ingredient>{
            val items = mutableListOf<Ingredient>()

            for(ingr in list){
                if(ingr.checked){
                    items.add(ingr)
                }
            }

            return items
        }

        fun searchAPI(search : Search,
                      onSuccess : (items: List<Ingredient>?) -> Unit,
                      onFail : (t: Throwable) -> Unit){ IngredientAPI.search(search, onSuccess, onFail) }

        fun getAll(category: Int, type: Int? = null): MutableList<Ingredient>{
            val list: MutableList<Ingredient> = ArrayList()
            val table = SetDB.TblMyIngredient
            val database: SQLiteDatabase = dbHelper.writableDatabase
            val columns: Array<String> = arrayOf(
                table.COL_ID,
                table.COL_NAME,
                table.COL_AMOUNT,
                table.COL_UNIT,
                table.COL_CATEGORY,
                table.COL_TYPE,
                table.COL_ONLINE
            )

            var where = ""
            if(category != Category.ALL)
                where += table.COL_CATEGORY + "= $category"

            type?.let {
                where += " AND " + table.COL_TYPE + "= $type"
            }

            val data = database.query(
                table.TABLE_NAME,
                columns,
                where,
                null,
                null,
                null,
                null
            )

            if(data.moveToFirst()){
                do {
                    val online = (data.getInt(data.getColumnIndex(table.COL_ONLINE))) == 1
                    val ingredient = Ingredient(
                        data.getInt(data.getColumnIndex(table.COL_ID)),
                        data.getString(data.getColumnIndex(table.COL_NAME)),
                        data.getInt(data.getColumnIndex(table.COL_AMOUNT)),
                        data.getInt(data.getColumnIndex(table.COL_CATEGORY)),
                        data.getInt(data.getColumnIndex(table.COL_UNIT)),
                        data.getInt(data.getColumnIndex(table.COL_TYPE)),
                        null, false,
                        online
                    )

                    list.add(ingredient)

                } while (data.moveToNext())
            }

            database.close()
            return list
        }

        fun get(id: Int) : Ingredient? {
            val table = SetDB.TblMyIngredient
            val database = dbHelper.writableDatabase

            val columns: Array<String> = arrayOf(
                table.COL_ID,
                table.COL_NAME,
                table.COL_AMOUNT,
                table.COL_UNIT,
                table.COL_CATEGORY,
                table.COL_TYPE,
                table.COL_ONLINE
            )

            var where = table.COL_ID + "= $id"

            val data = database.query(
                table.TABLE_NAME,
                columns,
                where,
                null,
                null,
                null,
                null
            )

            var ingredient: Ingredient? = null
            if(data.moveToFirst()){
                do {
                    val online = (data.getInt(data.getColumnIndex(table.COL_ONLINE))) == 1
                    ingredient = Ingredient(
                        data.getInt(data.getColumnIndex(table.COL_ID)),
                        data.getString(data.getColumnIndex(table.COL_NAME)),
                        data.getInt(data.getColumnIndex(table.COL_AMOUNT)),
                        data.getInt(data.getColumnIndex(table.COL_CATEGORY)),
                        data.getInt(data.getColumnIndex(table.COL_UNIT)),
                        data.getInt(data.getColumnIndex(table.COL_TYPE)),
                        null, false,
                        online
                    )

                } while (data.moveToNext())
            }

            database.close()
            return ingredient
        }

        fun getMyIngredients(id_user: Int, onSuccess: (items: List<Ingredient>?) -> Unit, onFail: (t: Throwable) -> Unit){
            IngredientAPI.getMine(id_user, onSuccess, onFail)
        }
    }

    /**
     * Inserta el ingrediente en sqlite
     */
    fun insert(): Boolean {
        val table = SetDB.TblMyIngredient
        val database: SQLiteDatabase = dbHelper.writableDatabase
        val values: ContentValues = ContentValues()
        var boolResult: Boolean = true

        values.put(table.COL_ID, this.id_ingredient)
        values.put(table.COL_NAME, this.name)
        values.put(table.COL_AMOUNT, this.amount)
        values.put(table.COL_UNIT, this.unit)
        values.put(table.COL_CATEGORY, this.category)
        values.put(table.COL_TYPE, this.type)
        values.put(table.COL_ONLINE, this.online)

        boolResult = try {
            val result = database.insertOrThrow(table.TABLE_NAME, null, values)

            result != (-1).toLong()
        } catch (e:SQLiteConstraintException){
            true
        }
        catch (e: Exception) {
            Log.e("Exception", e.toString())
            false
        }

        database.close()

        return boolResult
    }

    fun insertIntoRecipeIngredient(id_recipe: Int): Boolean{
        val table = SetDB.TblRecipeIngr
        val database: SQLiteDatabase = dbHelper.writableDatabase
        val values: ContentValues = ContentValues()
        var boolResult: Boolean = true

        values.put(table.COL_ID_RECIPE, id_recipe)
        values.put(table.COL_ID_INGR, this.id_ingredient)
        values.put(table.COL_INGR_NAME, this.name)

        boolResult = try{
            val result = database.insertOrThrow(table.TABLE_NAME, null, values)
            result != (-1).toLong()
        } catch (e: SQLiteConstraintException) {
            Log.e("Exception", e.toString())
            true
        } catch (e:Exception){
            Log.e("Exception", e.toString())
            false
        }

        return boolResult
    }

    /**
     * Guarda el ingrediente en la base de datos general.
     */
    fun save(onSuccess: (Int?) -> Unit, onFail: (t: Throwable) -> Unit){
        IngredientAPI.save(this, onSuccess, onFail)

    }

    fun update(): Boolean {
        val table = SetDB.TblMyIngredient
        val database: SQLiteDatabase = dbHelper.writableDatabase
        val values: ContentValues = ContentValues()
        var boolResult : Boolean = true

        values.put(table.COL_AMOUNT, this.amount)
        values.put(table.COL_TYPE, this.type)
        values.put(table.COL_CATEGORY, this.category)
        values.put(table.COL_UNIT, this.unit)

        val where = table.COL_ID + " = " + this.id_ingredient

        boolResult = try{
            val result = database.update(table.TABLE_NAME, values, where, null)

            result > 0
        } catch (e: Exception){
            Log.e("Exception", e.toString())
            false
        }

        return boolResult
    }

    fun update(onSuccess: (Int?) -> Unit, onFail: (t: Throwable) -> Unit){
        IngredientAPI.update(this, onSuccess, onFail)
    }

    /**
     * Setea el campo [online]
     */
    fun updateOnline(): Boolean {
        val table = SetDB.TblMyIngredient
        val database : SQLiteDatabase = dbHelper.writableDatabase
        val values: ContentValues = ContentValues()
        var boolResult: Boolean = true

        values.put(table.COL_ONLINE, this.online)

        val where = table.COL_ID + " = " + this.id_ingredient

        boolResult = try{
            val result = database.update(table.TABLE_NAME, values, where, null)

            result > 0
        } catch (e: Exception){
            Log.e("Exception", e.toString())
            false
        }

        return boolResult
    }

    fun remove() : Boolean {
        val table = SetDB.TblMyIngredient
        val database : SQLiteDatabase = dbHelper.writableDatabase
        var boolResult: Boolean = true

        try {
            val rows = database.delete(table.TABLE_NAME, "${table.COL_ID} = ${this.id_ingredient}", null)
             if(rows > 0) boolResult = true
        }
        catch (e: Exception){
            boolResult = false
        }

        return boolResult
    }

    fun remove(onSuccess: (Int?) -> Unit, onFail: (t: Throwable) -> Unit){
        IngredientAPI.remove(this, onSuccess, onFail)
    }
}
