package com.fcfm.fritch.models

import com.fcfm.fritch.api.UserAPI

data class User(
    var id_user: Int? = null,
    var name: String? = null,
    var id_account: Int? = null
){
    fun addAPI(onSuccess: (Int?) -> Unit, onFail:(t: Throwable) -> Unit){
        UserAPI.addUser(this, onSuccess, onFail)
    }

    fun updateAPI(onSuccess: (Int?) -> Unit, onFail: (t: Throwable) -> Unit){
        UserAPI.update(this, onSuccess, onFail)
    }
}
