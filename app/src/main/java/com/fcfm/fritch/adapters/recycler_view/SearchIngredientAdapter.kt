package com.fcfm.fritch.adapters.recycler_view

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.fcfm.fritch.R
import com.fcfm.fritch.interfaces.IRVOnClick
import com.fcfm.fritch.models.Ingredient
import com.fcfm.fritch.utils.Category
import kotlinx.android.synthetic.main.item_ingredient.view.*

class SearchIngredientAdapter(val context: Context,
                              val items: MutableList<Ingredient>,
                              val listener:IRVOnClick.Ingredient,
                              val category : Int) : RecyclerView.Adapter<SearchIngredientAdapter.ViewHolder>() {

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        fun assignData(ingredient: Ingredient, listener: IRVOnClick.Ingredient){

            itemView.iiName_txt.text = ingredient.name

            when(category){
                Category.SEASONING -> itemView.iiName_txt.setCompoundDrawablesWithIntrinsicBounds(context.getDrawable(R.drawable.ic_condiment), null, null, null)
                Category.CUPBOARD -> itemView.iiName_txt.setCompoundDrawablesWithIntrinsicBounds(context.getDrawable(R.drawable.ic_condiment), null, null, null)
                Category.FRIDGE -> itemView.iiName_txt.setCompoundDrawablesWithIntrinsicBounds(context.getDrawable(R.drawable.ic_condiment), null, null, null)

            }

            itemView.setOnClickListener { listener.onClick(ingredient) }
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_ingredient, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.assignData(items[position], listener)
    }

    override fun getItemCount(): Int = items.size
}