package com.fcfm.fritch.adapters

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.fcfm.fritch.fragments.recipes.View_ingredients
import com.fcfm.fritch.fragments.recipes.View_instructions

class ViewRecipeViewPageAdapter(fragment: Fragment, val id:Int, val downloaded: Boolean) : FragmentStateAdapter(fragment) {
    override fun getItemCount(): Int = 2

    override fun createFragment(position: Int): Fragment {
        val fragment: Fragment

        when(position){
            0 -> fragment = View_ingredients.newInstance(id, downloaded)
            1 -> fragment = View_instructions.newInstance(id, downloaded)
            else -> fragment = View_ingredients.newInstance(id, downloaded)
        }

        return fragment
    }
}