package com.fcfm.fritch.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.fcfm.fritch.R
import com.fcfm.fritch.interfaces.IRVOnClick
import com.fcfm.fritch.models.Ingredient
import kotlinx.android.synthetic.main.seasoning_item.view.*

class SeasoningListAdapter(val context: Context,
                           var seasoning:MutableList<Ingredient>,
                           val onClick: IRVOnClick.Ingredient
                           ): RecyclerView.Adapter<SeasoningListAdapter.ViewHolder>() {

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val x = 5

        fun assignData(Ingr:Ingredient, onClick: IRVOnClick.Ingredient){
            itemView.si_txtName.text = Ingr.name
            itemView.si_pbAmount.progress = Ingr.amount!!

            itemView.setOnClickListener{
                onClick.onClick(Ingr)
            }

            itemView.siDec_btn.setOnClickListener {
                Ingr.amount = Ingr.amount?.minus(x)
                itemView.si_pbAmount.progress = Ingr.amount!!
                Ingr.online = false
            }

            itemView.siDec_btn.setOnLongClickListener{
                Ingr.amount = itemView.si_pbAmount.min
                itemView.si_pbAmount.progress = Ingr.amount!!
                Ingr.online = false

                true
            }

            itemView.siInc_btn.setOnClickListener{
                Ingr.amount = Ingr.amount?.plus(x)
                itemView.si_pbAmount.progress = Ingr.amount!!
                Ingr.online = false
            }

            itemView.siInc_btn.setOnLongClickListener{
                Ingr.amount = itemView.si_pbAmount.max
                itemView.si_pbAmount.progress = Ingr.amount!!
                Ingr.online = false

                true
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.seasoning_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.assignData(seasoning[position], onClick)
    }

    override fun getItemCount(): Int = this.seasoning.size
}