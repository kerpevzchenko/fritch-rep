package com.fcfm.fritch.adapters

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.fcfm.fritch.fragments.recipes.My_recipes
import com.fcfm.fritch.fragments.recipes.Recipes
import com.fcfm.fritch.fragments.recipes.Search_recipes

class RecipesViewPageAdapter(fragment:Fragment) : FragmentStateAdapter(fragment) {
    override fun getItemCount(): Int = 2

    override fun createFragment(position: Int): Fragment {
        val fragment:Fragment

        when(position){
            0 -> fragment = Search_recipes()
            1 -> fragment = My_recipes()

            else -> fragment = Search_recipes()
        }

        return fragment
    }

}