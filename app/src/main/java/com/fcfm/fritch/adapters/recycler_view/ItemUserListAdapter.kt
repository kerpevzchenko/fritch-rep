package com.fcfm.fritch.adapters.recycler_view

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.fcfm.fritch.R
import com.fcfm.fritch.interfaces.IRVOnClick
import com.fcfm.fritch.models.User
import kotlinx.android.synthetic.main.item_user.view.*

class ItemUserListAdapter(val context: Context,
                          var items: MutableList<User>,
                          val listener: IRVOnClick.User): RecyclerView.Adapter<ItemUserListAdapter.ViewHolder>(){
    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        fun assingData(user:User, listener: IRVOnClick.User){
            itemView.iu_txt.text = user.name

            itemView.setOnClickListener { listener.onClick(user) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_user, parent,false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.assingData(items[position], listener)
    }

    override fun getItemCount(): Int = items.size
}