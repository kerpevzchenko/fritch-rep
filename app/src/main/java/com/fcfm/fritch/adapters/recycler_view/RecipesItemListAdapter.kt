package com.fcfm.fritch.adapters.recycler_view

import android.content.Context
import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.fcfm.fritch.R
import com.fcfm.fritch.interfaces.IRVOnClick
import com.fcfm.fritch.models.Recipe
import kotlinx.android.synthetic.main.recipe_item.view.*
import java.util.*

class RecipesItemListAdapter(val context:Context,
                             var items: MutableList<Recipe>,
                             val listener: IRVOnClick.Recipe) : RecyclerView.Adapter<RecipesItemListAdapter.ViewHolder>()
{
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        fun assingData(rec: Recipe, listener: IRVOnClick.Recipe){
            itemView.riName_txt.text = rec.name
            itemView.riPortions_txt.text = rec.portions.toString() + " " + context.getString(R.string.ri_portions)
            itemView.riType_txt.text = getTypeText(rec.type!!.toInt())

            val byteArray = Base64.getDecoder().decode(rec.img)
            itemView.riPic_iv.setImageBitmap(BitmapFactory.decodeByteArray(byteArray, 0, byteArray.size))

            itemView.setOnClickListener {
                listener.onClick(rec)
            }

        }

        private fun getTypeText(type:Int): String{
            return when(type){
                0 -> context.getString(R.string.rt_breakfast)
                1 -> context.getString(R.string.rt_meal)
                2 -> context.getString(R.string.rt_dinner)
                else -> context.getString(R.string.not_spec)
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.recipe_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.assingData(items[position], listener)
    }

    override fun getItemCount(): Int = items.size
}