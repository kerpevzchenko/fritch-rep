package com.fcfm.fritch.adapters

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.fcfm.fritch.fragments.fridge.Refrigerator

class FridgeViewPageAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {
    override fun getItemCount(): Int = 2

    override fun createFragment(position: Int): Fragment {
        val fragment = Refrigerator()

        fragment.arguments = Bundle().apply {
            putInt("object", position)
        }
        return fragment
    }
}