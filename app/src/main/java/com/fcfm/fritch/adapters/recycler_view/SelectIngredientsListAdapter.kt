package com.fcfm.fritch.adapters.recycler_view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.fcfm.fritch.R
import com.fcfm.fritch.models.Ingredient
import kotlinx.android.synthetic.main.item_select_ingredient.view.*

class SelectIngredientsListAdapter(val items: MutableList<Ingredient>, val selectable: Boolean = true): RecyclerView.Adapter<SelectIngredientsListAdapter.ViewHolder>() {

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        fun assignData(ingredient: Ingredient){
            itemView.isi_txt.text = ingredient.name
            itemView.isi_cb.isChecked = ingredient.checked

            if(selectable){
                itemView.setOnClickListener{
                    itemView.isi_cb.isChecked = !itemView.isi_cb.isChecked
                    ingredient.checked = itemView.isi_cb.isChecked
                }
            }
            else{
                itemView.isClickable = false
                itemView.isi_cb.visibility = View.INVISIBLE
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_select_ingredient, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.assignData(items[position])
    }

    override fun getItemCount(): Int = items.size
}