package com.fcfm.fritch.adapters.recycler_view

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.fcfm.fritch.R
import com.fcfm.fritch.interfaces.IRVOnClick
import com.fcfm.fritch.models.Ingredient
import kotlinx.android.synthetic.main.item_ingredient_02.view.*

class IngredientsListAdapter(val context: Context,
                             val items: MutableList<Ingredient>,
                             val listener: IRVOnClick.Ingredient,
                             val searchRecClick: (ingredient: Ingredient) -> Unit
) : RecyclerView.Adapter<IngredientsListAdapter.ViewHolder>() {

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        private val unitsArray: Array<String>? = context.resources.getStringArray( R.array.units )
        fun assingData(ingr: Ingredient, listener: IRVOnClick.Ingredient, searchRecClick: (ingredient: Ingredient) -> Unit){
            itemView.iiName_txt.text = ingr.name
            itemView.iiAmount_txt.text = ingr.amount.toString()
            itemView.iiUnit_txt.text = unitsArray!![ingr.unit!!]

            itemView.setOnClickListener{
                listener.onClick(ingr)
            }

            itemView.iiSearchRec_btn.setOnClickListener{
                searchRecClick(ingr)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_ingredient_02, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.assingData(items[position], listener, searchRecClick)
    }

    override fun getItemCount(): Int = items.size
}